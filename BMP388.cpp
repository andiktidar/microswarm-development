/*
 * BMP388.cpp
 *
 *  Created on: 20.02.2019
 *      Author: Leon
 */

#include "BMP388.h"
#include "nrf_delay.h"

BMP388::BMP388(I2CControl* i2CControlHandle) {
	i2c = i2CControlHandle;
	initCounter = 0;
}

BMP388::~BMP388() {
}

void BMP388::init(){
	i2c->write8(0x76, 0x7e, 0xb6);
	nrf_delay_ms(100);
	//iir
	i2c->write8(0x76, 0x1f, 0x04);
	//res
	i2c->write8(0x76, 0x1c, 0x0c);
	//odr
	i2c->write8(0x76, 0x1d, 0x03);
	//mode
	i2c->write8(0x76, 0x1b, 0x33);

	initCalibParameters();
}

/**
 * init calibration parameters according to the datasheet appendix 9
 */
void BMP388::initCalibParameters(){
	calibDataSet.parT1 = i2c->read16u(0x76,0x31)/0.00390625;
	calibDataSet.parT2 = i2c->read16u(0x76,0x33)/1073741824.0;
	calibDataSet.parT3 = i2c->read8(0x76,0x35)/281474976710656.0;


	calibDataSet.parP1 = (i2c->read16(0x76,0x36)-16384)/1048576.0;
	calibDataSet.parP2 = (i2c->read16(0x76,0x38)-16384)/536870912.0;
	calibDataSet.parP3 = i2c->read8(0x76,0x3a)/4294967296.0;

	calibDataSet.parP4 = i2c->read8(0x76,0x3b)/137438953472.0;
	calibDataSet.parP5 = i2c->read16u(0x76,0x3c)/0.125;
	calibDataSet.parP6 = i2c->read16u(0x76,0x3e)/64.0;

	calibDataSet.parP7 = i2c->read8(0x76,0x40)/256.0;
	calibDataSet.parP8 = i2c->read8(0x76,0x41)/32768.0;
	calibDataSet.parP9 = i2c->read16(0x76,0x42)/281474976710656.0;

	calibDataSet.parP10 = i2c->read8(0x76,0x44)/281474976710656.0;
	calibDataSet.parP11 = i2c->read8(0x76,0x45)/36893488147419103232.0;

}

void BMP388::update(){
	currentTempRaw = (i2c->read8u(0x76,0x09)<<16);
	currentTempRaw += (i2c->read8u(0x76,0x08)<<8);
	currentTempRaw += i2c->read8u(0x76,0x07);

	currentPresRaw = (i2c->read8u(0x76,0x06)<<16);
	currentPresRaw += (i2c->read8u(0x76,0x05)<<8);
	currentPresRaw += i2c->read8u(0x76,0x04);

	calcTemperature();
	calcPressure();
}

int BMP388::getCurrentMeasurementValue(){
	return currentTempRaw;
}

void BMP388::calcTemperature(){
	double partialData1, partialData2;

	partialData1 = (double)(currentTempRaw-calibDataSet.parT1);
	partialData2 = (double)(partialData1 * calibDataSet.parT2);

	temperature = partialData2 + (partialData1*partialData1) * calibDataSet.parT3;

	//temperature = 22.0;
}


void BMP388::calcPressure(){
	double partialData1, partialData2, partialData3, partialData4;
	double partialOut1, partialOut2;
	double result;

	partialData1 = calibDataSet.parP6 * temperature;
	partialData2 = calibDataSet.parP7 * (temperature*temperature);
	partialData3 = calibDataSet.parP8 * (temperature*temperature*temperature);
	partialOut1 = calibDataSet.parP5 + partialData1 + partialData2 + partialData3;

	partialData1 = calibDataSet.parP2 * temperature;
	partialData2 = calibDataSet.parP3 * (temperature*temperature);
	partialData3 = calibDataSet.parP4 * (temperature*temperature*temperature);
	partialOut2 = (double) currentPresRaw * (calibDataSet.parP1 + partialData1 + partialData2 + partialData3);

	partialData1 = (double) currentPresRaw * (double) currentPresRaw;
	partialData2 = calibDataSet.parP9 + calibDataSet.parP10 * temperature;
	partialData3 = partialData1 * partialData2;
	partialData4 = partialData3 + ((double)currentPresRaw * (double)currentPresRaw * (double)currentPresRaw) * calibDataSet.parP11;

	pressure = partialOut1+partialOut2+partialData4;
}


double BMP388::getTemperature(){
	return temperature;
}

double BMP388::getPressure(){
	return pressure;
}

