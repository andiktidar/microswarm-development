/*
 * DroneStatus.h
 *
 *  Created on: 18.01.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_DRONESTATUS_H_
#define MICROSWARM_V1_DRONESTATUS_H_

enum DroneStatus{
	UNDEFINED,
	INIT_UNKNOWN,
	READY,
	EMERGENCY
};


#endif /* MICROSWARM_V1_DRONESTATUS_H_ */
