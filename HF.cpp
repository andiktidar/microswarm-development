/*
 * HF.cpp
 *
 *  Created on: 08.02.2019
 *      Author: Leon
 */

#include "HF.h"

float HF::clamp(float value, float min, float max){
	return (value > min ? (value < max ? value : max) : min);
}
