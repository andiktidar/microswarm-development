#include "nrf_drv_twi.h"
#include "vl53l0x_i2c_platform.h"
#include "vl53l0x_platform.h"
#include "nrf_log.h"
#include "app_util_platform.h"
#include "I2CControlTransfer.h"

#define TWI_SCL_M                9   //!< Master SCL pin
#define TWI_SDA_M                8   //!< Master SDA pin

/**
 * @brief TWI master instance
 *
 * Instance of TWI master driver that would be used for communication with VL53L0X.
 */
//extern I2CControl* vl53_i2c;

/**
 * @brief  Initialise platform comms.
 *
 * @param  comms_type      - selects between I2C and SPI
 * @param  comms_speed_khz - unsigned short containing the I2C speed in kHz
 *
 * @return status - status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_comms_initialise(uint8_t  comms_type, uint16_t comms_speed_khz){

	//stump since communication in this project is already established once this code is called
	return 0x00;
};

/**
 * @brief  Close platform comms.
 *
 * @return status - status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_comms_close(void){
	//stump since the communication status is handled externally in this project
	return 0;
}

/**
 * @brief Writes the supplied byte buffer to the device
 *
 * Wrapper for SystemVerilog Write Multi task
 *
 * @code
 *
 * Example:
 *
 * uint8_t *spad_enables;
 *
 * int status = VL53L0X_write_multi(RET_SPAD_EN_0, spad_enables, 36);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  pdata - pointer to uint8_t buffer containing the data to be written
 * @param  count - number of bytes in the supplied byte buffer
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_write_multi(uint8_t address, uint8_t index, uint8_t  *pdata, int32_t count)
{
	ret_code_t ret = 0;

	/* device supports only limited number of bytes written in sequence */
	/*if (count > (VL53L0X_MAX_STRING_LENGTH_PLT))
	{
		return NRF_ERROR_INVALID_LENGTH;
	}*/

	/* All written data has to be in the same page */
	/*if ((address / (VL53L0X_MAX_STRING_LENGTH_PLT)) != ((index + count - 1) / (VL53L0X_MAX_STRING_LENGTH_PLT)))
	{
		return NRF_ERROR_INVALID_ADDR;
	}*/

	//for(int i = 0; i < count; i++){
		vl53_i2c->writeLength(address,index,pdata,count);
	//}

	/*do{
		uint8_t buffer[1 + VL53L0X_MAX_STRING_LENGTH_PLT]; // index + data
		buffer[0] = (uint8_t)index;
		memcpy(buffer + 1, pdata, count);
		ret = nrf_drv_twi_tx(&m_twi_master, address >> 1, buffer, count + 1, false);
	} while (0);*/
	return ret;
}

/**
 * @brief  Reads the requested number of bytes from the device
 *
 * Wrapper for SystemVerilog Read Multi task
 *
 * @code
 *
 * Example:
 *
 * uint8_t buffer[COMMS_BUFFER_SIZE];
 *
 * int status = status  = VL53L0X_read_multi(DEVICE_ID, buffer, 2)
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  pdata - pointer to the uint8_t buffer to store read data
 * @param  count - number of uint8_t's to read
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_read_multi(uint8_t address,  uint8_t index, uint8_t  *pdata, int32_t count)
{
    ret_code_t ret = 0;

    if (count > (VL53L0X_MAX_STRING_LENGTH_PLT))
    {
        return NRF_ERROR_INVALID_LENGTH;
    }

    vl53_i2c->readLength(address,index,pdata,count);
    return ret;
}

/**
 * @brief  Writes a single byte to the device
 *
 * Wrapper for SystemVerilog Write Byte task
 *
 * @code
 *
 * Example:
 *
 * uint8_t page_number = MAIN_SELECT_PAGE;
 *
 * int status = VL53L0X_write_byte(PAGE_SELECT, page_number);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  data  - uint8_t data value to write
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */


int32_t VL53L0X_write_byte(uint8_t address,  uint8_t index, uint8_t data)
{
    ret_code_t ret = 0;
    vl53_i2c->write8(address,index,data);
    return ret;

}

/**
 * @brief  Writes a single word (16-bit unsigned) to the device
 *
 * Manages the big-endian nature of the device (first byte written is the MS byte).
 * Uses SystemVerilog Write Multi task.
 *
 * @code
 *
 * Example:
 *
 * uint16_t nvm_ctrl_pulse_width = 0x0004;
 *
 * int status = VL53L0X_write_word(NVM_CTRL__PULSE_WIDTH_MSB, nvm_ctrl_pulse_width);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  data  - uin16_t data value write
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_write_word(uint8_t address,  uint8_t index, uint16_t  data)
{
    ret_code_t ret = 0;

    unsigned char tempBuf[2];
    tempBuf[0] = data << 8;
    tempBuf[1] = data;

    vl53_i2c->writeLength(address,index,tempBuf,2);

    return ret;

}

/**
 * @brief  Writes a single dword (32-bit unsigned) to the device
 *
 * Manages the big-endian nature of the device (first byte written is the MS byte).
 * Uses SystemVerilog Write Multi task.
 *
 * @code
 *
 * Example:
 *
 * uint32_t nvm_data = 0x0004;
 *
 * int status = VL53L0X_write_dword(NVM_CTRL__DATAIN_MMM, nvm_data);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  data  - uint32_t data value to write
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_write_dword(uint8_t address, uint8_t index, uint32_t  data)
{
    ret_code_t ret = 0;

    /*vl53_i2c->write8(address,index+3,data<<24);
    vl53_i2c->write8(address,index+2,data<<16);
    vl53_i2c->write8(address,index+1,data<<8);
    vl53_i2c->write8(address,index,data);*/

    unsigned char tempBuf[4];
    tempBuf[0] = data << 24;
    tempBuf[1] = data << 16;
    tempBuf[2] = data << 8;
    tempBuf[3] = data;

    vl53_i2c->writeLength(address,index,tempBuf,4);

    return ret;
}

/**
 * @brief  Reads a single byte from the device
 *
 * Uses SystemVerilog Read Byte task.
 *
 * @code
 *
 * Example:
 *
 * uint8_t device_status = 0;
 *
 * int status = VL53L0X_read_byte(STATUS, &device_status);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index  - uint8_t register index value
 * @param  pdata  - pointer to uint8_t data value
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_read_byte(uint8_t address,  uint8_t index, uint8_t  *pdata)
{
    ret_code_t ret = 0;
    *pdata = vl53_i2c->read8u(address,index);
    return ret;
}

/**
 * @brief  Reads a single word (16-bit unsigned) from the device
 *
 * Manages the big-endian nature of the device (first byte read is the MS byte).
 * Uses SystemVerilog Read Multi task.
 *
 * @code
 *
 * Example:
 *
 * uint16_t timeout = 0;
 *
 * int status = VL53L0X_read_word(TIMEOUT_OVERALL_PERIODS_MSB, &timeout);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index  - uint8_t register index value
 * @param  pdata  - pointer to uint16_t data value
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_read_word(uint8_t address,  uint8_t index, uint16_t *pdata)
{
    ret_code_t ret = 0;

    unsigned char temp[2];

    vl53_i2c->readLength(address,index,temp,2);

    *pdata = temp[1]+(temp[0]<<8);

    return ret;
}

/**
 * @brief  Reads a single dword (32-bit unsigned) from the device
 *
 * Manages the big-endian nature of the device (first byte read is the MS byte).
 * Uses SystemVerilog Read Multi task.
 *
 * @code
 *
 * Example:
 *
 * uint32_t range_1 = 0;
 *
 * int status = VL53L0X_read_dword(RANGE_1_MMM, &range_1);
 *
 * @endcode
 *
 * @param  address - uint8_t device address value
 * @param  index - uint8_t register index value
 * @param  pdata - pointer to uint32_t data value
 *
 * @return status - SystemVerilog status 0 = ok, 1 = error
 *
 */

int32_t VL53L0X_read_dword(uint8_t address, uint8_t index, uint32_t *pdata)
{
    ret_code_t ret = 0;

    //*pdata = vl53_i2c->read32u(address,index);

    unsigned char temp[4];

    vl53_i2c->readLength(address,index,temp,4);

    *pdata = temp[3]+(temp[2]<<8)+(temp[1]<<16)+(temp[0]<<24);

    return ret;
}


