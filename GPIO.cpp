/*
 * ControllerConfiguration.cpp
 *
 *  Created on: 17.01.2019
 *      Author: Leon
 */

#include "GPIO.h"
#include "nrf_gpio.h"


void GPIO::initBase(){
	nrf_gpio_cfg_output(LED0);
	nrf_gpio_pin_clear(LED0);
	nrf_gpio_cfg_output(LED1);
	nrf_gpio_pin_clear(LED1);
	nrf_gpio_cfg_output(LED2);
	nrf_gpio_pin_clear(LED2);

	nrf_gpio_cfg_output(US_MODULE0_PWR);
	nrf_gpio_pin_set(US_MODULE0_PWR);
	nrf_gpio_cfg_output(US_MODULE1_PWR);
	nrf_gpio_pin_set(US_MODULE1_PWR);

	nrf_gpio_cfg_input(BUTTON_ENTER, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(BUTTON_BACK, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(BUTTON_PLUS, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(BUTTON_MINUS, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(DIP0, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(DIP1, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(DIP2, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(DIP3, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(DIP4, NRF_GPIO_PIN_PULLUP);
	nrf_gpio_cfg_input(DIP5, NRF_GPIO_PIN_PULLUP);
}


bool GPIO::isPressed(unsigned int pin){
	return (!nrf_gpio_pin_read(pin));
}

