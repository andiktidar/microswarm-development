/*
 * BLEHandler.cpp
 *
 *  Created on: 19.11.2018
 *      Author: Leon
 */

#include "BLEHandler.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "nrf_ble_gatt.h"
#include "ble_radio_notification.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_log.h"
#include <stdio.h>
#include "Err.h"
#include "BLEMessageType.h"

#define BLE_LED 16

NRF_BLE_SCAN_DEF(scanHandle);

unsigned char BLEHandler::deviceKey[2];
ble_gap_adv_data_t BLEHandler::encodedMessage;
ble_advdata_t BLEHandler::advDataConfig;
ble_advdata_manuf_data_t BLEHandler::msgData;
unsigned char BLEHandler::msgDataArray[29];
ble_gap_adv_params_t BLEHandler::advParamSet;
unsigned char BLEHandler::resultingDataset[31];
uint8_t BLEHandler::advHandle;
uint8_t BLEHandler::error;
ble_gap_scan_params_t BLEHandler::scanParams;
nrf_ble_scan_init_t BLEHandler::scanInitSet;
bool BLEHandler::currentlySending = false;
ble_gap_addr_t BLEHandler::addressFilter;
bool BLEHandler::initialized = false;
unsigned char BLEHandler::receivedData[32];

void* BLEHandler::transmitEventCallback = (void*)callbackDummy;
void* BLEHandler::receiveEventCallback = (void*)callbackDummy;

void BLEHandler::TXHandler(bool radioEvent) {
	//if (!radioEvent) {
		if(currentlySending){
			nrf_gpio_pin_clear(BLE_LED);
			((void (*)(void)) transmitEventCallback)();
			BLEHandler::endTransmission();
		}
	//}
}

void BLEHandler::RXHandler(ble_evt_t const * p_ble_evt, void * p_context) {

	//nrf_gpio_pin_toggle(16);
	switch (p_ble_evt->header.evt_id) {
	case BLE_GAP_EVT_ADV_REPORT: {
		uint8_t len = p_ble_evt->evt.gap_evt.params.adv_report.data.len;
		uint8_t *dataPtr = p_ble_evt->evt.gap_evt.params.adv_report.data.p_data;
		if (len > 3 && dataPtr[3] == (0x13 + (dataPtr[2] << 3))) {
			//copy the message frame to
			for(int count = 0; count < 32; count++){
				receivedData[count] = dataPtr[count];
			}
			((void (*)(void)) receiveEventCallback)();
		}
		break;
	}

	default:
		break;
	}
}

void BLEHandler::callbackDummy(){

}

void BLEHandler::setCBTFunction(void* cb){
	transmitEventCallback = cb;
}

void BLEHandler::setCBRFunction(void* cb){
	receiveEventCallback = cb;
}

void BLEHandler::SEHandler(scan_evt_t const * p_scan_evt) {
}

void BLEHandler::init(unsigned char key, void* cbr, void* cbt) {

	if(!initialized){
		//set the BLE led pin
		nrf_gpio_cfg_output(BLE_LED);
		nrf_gpio_pin_clear(BLE_LED);

		Err::signal(nrf_sdh_enable_request());
		Err::signal(ble_radio_notification_init(6, NRF_RADIO_NOTIFICATION_DISTANCE_800US, BLEHandler::TXHandler));

		nrf_delay_ms(10);

		uint32_t ram_start = 0x00;
		Err::signal(nrf_sdh_ble_default_cfg_set(1, &ram_start));
		Err::signal(nrf_sdh_ble_enable(&ram_start));

		advHandle = BLE_GAP_ADV_SET_HANDLE_NOT_SET;

		changeID(key);

		//advertisement data package configuration (configured to be the bare minimum for a bigger custom data set)
		memset(&advDataConfig, 0, sizeof(advDataConfig));
		advDataConfig.include_ble_device_addr = false;
		advDataConfig.include_appearance = false;
		advDataConfig.flags = 0x00;
		advDataConfig.p_tx_power_level = NULL;
		advDataConfig.uuids_more_available.uuid_cnt = 0;
		advDataConfig.uuids_complete.uuid_cnt = 0;
		advDataConfig.uuids_solicited.uuid_cnt = 0;
		advDataConfig.p_slave_conn_int = NULL;
		advDataConfig.p_manuf_specific_data = &msgData;
		advDataConfig.service_data_count = 0;
		advDataConfig.name_type = BLE_ADVDATA_NO_NAME;

		//set the company identifier to be the device id (better use of the 29 bytes available)
		memset(&msgData, 0, sizeof(msgData));
		msgData.company_identifier = (deviceKey[1] << 8) + deviceKey[0];
		msgData.data.p_data = msgDataArray;

		//set advertising parameters
		memset(&advParamSet, 0, sizeof(advParamSet));

		advParamSet.properties.type =
		BLE_GAP_ADV_TYPE_NONCONNECTABLE_NONSCANNABLE_UNDIRECTED; // Undirected advertisement.
		advParamSet.p_peer_addr = NULL;
		advParamSet.filter_policy = BLE_GAP_ADV_FP_ANY;
		advParamSet.interval = 1600;
		advParamSet.duration = 0;

		encodedMessage.adv_data.p_data = resultingDataset;
		encodedMessage.adv_data.len = 30;
		Err::signal(sd_ble_gap_adv_set_configure(&advHandle, &encodedMessage, &advParamSet));

		//enable scanning module
		memset(&scanParams, 0, sizeof(scanParams));

		scanParams.active = 0;
		scanParams.extended = 0;
		scanParams.report_incomplete_evts = 0;
		scanParams.filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL;
		scanParams.interval = 160;  //4;
		scanParams.scan_phys = BLE_GAP_PHY_1MBPS;
		scanParams.window = 160;  //4;
		scanParams.timeout = 0;
		scanParams.channel_mask[0] = 0;
		scanParams.channel_mask[1] = 0;
		scanParams.channel_mask[2] = 0;
		scanParams.channel_mask[3] = 0;
		scanParams.channel_mask[4] = 0x60;

		// Register a handler for BLE and SoC events.
		NRF_SDH_BLE_OBSERVER(m_ble_observer, 3, BLEHandler::RXHandler, NULL);

		memset(&scanInitSet, 0, sizeof(scanInitSet));

		scanInitSet.connect_if_match = false;
		scanInitSet.conn_cfg_tag = 1;
		scanInitSet.p_scan_param = &scanParams;

		Err::signal(nrf_ble_scan_init(&scanHandle, &scanInitSet, BLEHandler::SEHandler));

		/*addressFilter.addr[0] = 0xca;
		addressFilter.addr[1] = 0x36;
		addressFilter.addr[2] = 0xbf;
		addressFilter.addr[3] = 0x5a;
		addressFilter.addr[4] = 0xb1;
		addressFilter.addr[5] = 0x41;
		addressFilter.addr_type = BLE_GAP_ADDR_TYPE_PUBLIC;

		Err::signal(nrf_ble_scan_filter_set(&scanHandle, SCAN_ADDR_FILTER, addressFilter.addr));
		Err::signal(nrf_ble_scan_filters_enable(&scanHandle, NRF_BLE_SCAN_ADDR_FILTER, false));*/

		//
		Err::signal(nrf_ble_scan_start(&scanHandle));
	}

}

void BLEHandler::changeID(unsigned char newID){

	//device BLE id calculation
	deviceKey[0] = newID;
	deviceKey[1] = 0x13 + (newID << 3);
	msgData.company_identifier = (deviceKey[1] << 8) + deviceKey[0];
}

void BLEHandler::send(unsigned char* data, unsigned char length) {
	nrf_ble_scan_stop();
	//set msgData size
	msgData.data.size = length;
	encodedMessage.adv_data.len = 30;

	//fill msgData array with the desired message
	for (int count = 0; count < length; count++) {
		msgDataArray[count] = data[count];
	}

	Err::signalWithCode(ble_advdata_encode(&advDataConfig, encodedMessage.adv_data.p_data, &encodedMessage.adv_data.len),1);
	currentlySending = true;
	/*Err::signalWithCode(*/sd_ble_gap_adv_start(advHandle, 1);//,2);
	nrf_gpio_pin_set(BLE_LED);
}

void BLEHandler::endTransmission() {
	Err::signalWithCode(sd_ble_gap_adv_stop(advHandle),3);
	Err::signalWithCode(nrf_ble_scan_start(&scanHandle),4);
	currentlySending = false;
}

unsigned char* BLEHandler::getMessageData(){
	return &receivedData[0];
}

void BLEHandler::sendDebugFloat(float value){
	unsigned char message[5];
	unsigned char* dataPtr = (unsigned char*)&value;

	message[0] = BLEMessageType::DEBUG_FLOAT;

	for(int count = 0; count < 4; count++){
		message[count+1] = *dataPtr;
		dataPtr++;
	}

	/*message[1] = *(dataPtr+3);
	message[2] = *(dataPtr+2);
	message[3] = *(dataPtr+1);
	message[4] = *(dataPtr);*/

	send(message,5);
}

void BLEHandler::sendDebugInt(int value){
	unsigned char message[5];
	unsigned char* dataPtr = (unsigned char*)&value;

	message[0] = BLEMessageType::DEBUG_INT;

	for(int count = 0; count < 4; count++){
		message[count+1] = *dataPtr;
		dataPtr++;
	}

	send(message,5);
}


void BLEHandler::sendDebugFull(float mx, float my, float vx, float vy, float sx, float sy){
	unsigned char message[25];

	message[0] = BLEMessageType::DEBUG_FULL;

	floatToCharArray(mx,(message+1));
	floatToCharArray(my,(message+5));
	floatToCharArray(vx,(message+9));
	floatToCharArray(vy,(message+13));
	floatToCharArray(sx,(message+17));
	floatToCharArray(sy,(message+21));
	send(message,25);
}

void BLEHandler::sendDebugDistanceAndAngle(float distance, float angle){
	unsigned char message[9];

	message[0] = BLEMessageType::DEBUG_DIST_ANGLE;

	floatToCharArray(distance,(message+1));
	floatToCharArray(angle,(message+5));

	send(message,9);
}

void BLEHandler::floatToCharArray(float value, unsigned char* data){
	unsigned char* dataPtr = (unsigned char*)&value;

	for(int count = 0; count < 4; count++){
		*data = *dataPtr;
		dataPtr++;
		data++;
	}
}
