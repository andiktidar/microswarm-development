/*
 * ConfigCalculator.cpp
 *
 *  Created on: 11.12.2018
 *      Author: Leon
 */

#include "ConfigCalculator.h"


unsigned char ConfigCalculator::calcFrequencyByte(unsigned char khz){
	if(khz > 80)
		khz = 80;
	if(khz < 30)
		khz = 30;

	return 30+(khz-30)*5;
}

unsigned char ConfigCalculator::calcBurstCyclesByte(unsigned char numberOfBurstCycles){
	if(numberOfBurstCycles > 31)
		numberOfBurstCycles = 31;

	return numberOfBurstCycles;
}

unsigned char ConfigCalculator::calcRecordTimeByte(unsigned char recTimeMS){
	unsigned char result = 0x00;
	result += (recTimeMS/4);
	result += (result << 4);
	return result;
}
