/*
 * ControlMultiplexer.h
 *
 *  Created on: 07.02.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_CONTROLMULTIPLEXER_H_
#define MICROSWARM_V1_CONTROLMULTIPLEXER_H_

#include "UARTHandler.h"

#define NUM_CHANNELS 14

class ControlMultiplexer {
public:
	ControlMultiplexer(UARTHandler* u);
	~ControlMultiplexer();

	void setArm(bool armStatus);
	void clearArm();
	void setMode(unsigned char mode);
	void clearMode();
	void setChannel(unsigned char channelIndex, float value);
	float getChannel(unsigned char channelIndex);
	void freeChannel(unsigned char channelIndex);
	void setRaw(short value, unsigned short mask);
	void clearRaw(unsigned short mask);
	void readControlInput();

	void pushControlMessage();

	static void uartRXCallback(const nrfx_uarte_event_t *event, void* context);
	static ControlMultiplexer* controlInstance;
	static bool workingOnMessage;

private:
	short controlInput[NUM_CHANNELS];
	short controlOverrideValues[NUM_CHANNELS];

	unsigned char readBuffer[32];
	unsigned char writeBuffer[32];

	unsigned short overrideMask;
	unsigned char readChannelIndex;

	UARTHandler* uart;
};

#endif /* MICROSWARM_V1_CONTROLMULTIPLEXER_H_ */
