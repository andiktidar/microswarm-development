/*
 * ControlMultiplexer.cpp
 *
 *  Created on: 07.02.2019
 *      Author: Leon
 */

#include "ControlMultiplexer.h"

#include <nrf_log.h>
#include <nrfx_uarte.h>
#include "HF.h"


ControlMultiplexer* ControlMultiplexer::controlInstance = nullptr;
bool ControlMultiplexer::workingOnMessage = false;

ControlMultiplexer::ControlMultiplexer(UARTHandler* u) {
	overrideMask = 0x0000;
	uart = u;
	controlInstance = this;
	readChannelIndex = 0;

	for(int count = 0; count < NUM_CHANNELS; count++){
		controlInput[NUM_CHANNELS] = 1500;
		controlOverrideValues[NUM_CHANNELS] = 1500;

	}

	controlInput[2] = 1000;
	controlOverrideValues[2] = 1000;

	uart->setRXCallback((void*)uartRXCallback);
	uart->read(&readBuffer[0],32);
}

ControlMultiplexer::~ControlMultiplexer() {
	// TODO Auto-generated destructor stub
}


void ControlMultiplexer::uartRXCallback(const nrfx_uarte_event_t* event, void* context){
	switch(event->type){
	case NRFX_UARTE_EVT_ERROR:
		controlInstance->uart->read(&controlInstance->readBuffer[0],32);
		break;
	case NRFX_UARTE_EVT_RX_DONE:
		controlInstance->readControlInput();
		controlInstance->pushControlMessage();
		//controlInstance->uart->write(&controlInstance->readBuffer[0], 32);
		//NRF_LOG_INFO("received something");
		break;
	case NRFX_UARTE_EVT_TX_DONE:
		controlInstance->uart->read(&controlInstance->readBuffer[0],32);
		//NRF_LOG_INFO("sent something");
		break;
	};
}


void ControlMultiplexer::setRaw(short value, unsigned short mask){
	overrideMask |= mask;
	unsigned char channelIndex;
	for(channelIndex = 0; channelIndex < NUM_CHANNELS && mask != 0x0001; channelIndex++){
		mask = (mask >> 1);
	}
	controlOverrideValues[channelIndex] = value;
}

void ControlMultiplexer::clearRaw(unsigned short mask){
	overrideMask &= (~mask);
}

float ControlMultiplexer::getChannel(unsigned char channelIndex){
	return (controlInput[channelIndex]-1000)/500.0f-1.0f;
}

/**
 * float values are between -1.0f and 1.0f
 */
void ControlMultiplexer::setChannel(unsigned char channelIndex, float value){

	value = HF::clamp(value, -1.0f, 1.0f);
	unsigned short rawValue = 1000+((value+1.0f)*500.0f);
	unsigned short rawChannel = 0x0001 << channelIndex;

	setRaw(rawValue, rawChannel);
}

void ControlMultiplexer::freeChannel(unsigned char channelIndex){
	clearRaw(0x0001 << channelIndex);
}

void ControlMultiplexer::pushControlMessage(){
	writeBuffer[0] = 0x20;
	writeBuffer[1] = 0x40;

	for(unsigned char count = 0; count < NUM_CHANNELS; count++){

		if((overrideMask >> count) & 0x01){
			writeBuffer[count*2+2] = (unsigned char)controlOverrideValues[count];
			writeBuffer[count*2+3] = (unsigned char)(controlOverrideValues[count] >> 8);
			//writeBuffer[count*2+2] = readBuffer[count*2+2];
			//writeBuffer[count*2+3] = readBuffer[count*2+3];
		}else{
			//writeBuffer[count*2+2] = readBuffer[count*2+2];
			//writeBuffer[count*2+3] = readBuffer[count*2+3];

			writeBuffer[count*2+2] = (unsigned char)controlInput[count];
			writeBuffer[count*2+3] = (unsigned char)(controlInput[count] >> 8);
		}

	}

	unsigned short checkSum = 0xffff;

	for(unsigned char count = 0; count < 30; count++){
		checkSum -= writeBuffer[count];
	}

	writeBuffer[30] = (unsigned char) checkSum;
	writeBuffer[31] = (unsigned char)(checkSum >> 8);

	//NRF_LOG_HEXDUMP_INFO(&writeBuffer[0],32);

	uart->write(&writeBuffer[0], 32);
}

void ControlMultiplexer::readControlInput(){
	for(int count = 0; count < 32; count+=2){
		if(readBuffer[count] == 0x20 && readBuffer[count+1] == 0x40){
			readChannelIndex = 0;
		}else{
			if(readChannelIndex < NUM_CHANNELS){
				controlInput[readChannelIndex] = readBuffer[count]+(readBuffer[count+1] << 8);
			}
			readChannelIndex++;
		}
	}
}




