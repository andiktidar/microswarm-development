#pragma once
template<class T>
class PIDControl
{
public:
	PIDControl(float p, float i, float d, float t, float awu):
		pFactor(p),
		iFactor(i),
		dFactor(d),
		tFactor(t),
		antiWindUp(awu),
		pVal(0),
		iVal(0),
		dVal(0),
		firstValue(true)
	{}

	~PIDControl() {};

	void update(T value, T target, float deltaTime)
	{
		T deviation = target - value;
		pVal = deviation * pFactor;
		iVal += deviation * iFactor*deltaTime;
		if(iVal > antiWindUp){
			iVal = antiWindUp;
		}
		else if(iVal < -antiWindUp){
			iVal = -antiWindUp;
		}

		if (!firstValue) {
			dVal = (deviation - lastDeviation) * dFactor*deltaTime;
		}
		else {
			firstValue = false;
			dVal = 0;
		}

		lastDeviation = deviation;
	}

	/*void update(T value, T target){
		update(value,target,tFactor);
	}*/

	T getPIDValue()
	{
		return pVal + iVal + dVal;
	}

	T getDeviation() {
		return lastDeviation;
	}

	void reset(){
		pVal = 0;
		iVal = 0;
		dVal = 0;
		firstValue = true;
	}

private:
	float pFactor, iFactor, dFactor, tFactor, antiWindUp;
	T pVal, iVal, dVal;
	T lastDeviation;
	bool firstValue;
};

