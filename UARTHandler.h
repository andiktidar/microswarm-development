/*
 * UARTHandler.h
 *
 *  Created on: 10.11.2018
 *      Author: Leon
 */

#ifndef BLE_APP_HRS_UARTHANDLER_H_
#define BLE_APP_HRS_UARTHANDLER_H_

#include "nrf.h"
//#include "nrf_drv_uarte.h"
#include "nrfx_uarte.h"
#include "Ringbuffer.h"

class UARTHandler {
public:
	UARTHandler(unsigned char instance);
	UARTHandler(unsigned char instance, unsigned char txp, unsigned char rxp, nrf_uarte_baudrate_t);
	~UARTHandler();
	void write(unsigned char* data, unsigned char length);
	unsigned char read(unsigned char* data, unsigned char length);
	void setRXCallback(void* cbFunction);

	static void readCBFunction(nrfx_uarte_event_t const* event, void* context);
	static UARTHandler* UARTContext;

private:
	nrfx_uarte_t handle;
	nrfx_uarte_config_t config;
	//Ringbuffer ringbuf;
	bool rxDone, txDone;


};

#endif /* BLE_APP_HRS_UARTHANDLER_H_ */
