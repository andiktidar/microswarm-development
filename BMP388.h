/*
 * BMP388.h
 *
 *  Created on: 20.02.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_BMP388_H_
#define MICROSWARM_V1_BMP388_H_

#include "I2CControl.h"

class BMP388 {
public:
	BMP388(I2CControl* i2CControlHandle);
	virtual ~BMP388();

	struct CalibDataSet{
		double parT1;
		double parT2;
		double parT3;

		double parP1;
		double parP2;
		double parP3;
		double parP4;

		double parP5;
		double parP6;
		double parP7;
		double parP8;

		double parP9;
		double parP10;
		double parP11;
	};

	void init();
	void update();
	float getHeight();
	int getCurrentMeasurementValue();
	double getTemperature();
	double getPressure();

	//TODO: only public for testing
	I2CControl* i2c;

private:

	void initCalibParameters();
	void calcTemperature();
	void calcPressure();

	CalibDataSet calibDataSet;
	unsigned int currentTempRaw, currentPresRaw;
	unsigned char initCounter;

	double temperature, pressure;

};

#endif /* MICROSWARM_V1_BMP388_H_ */
