/*
 * Err.cpp
 *
 *  Created on: 20.11.2018
 *      Author: Leon
 */

#include "Err.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"

void Err::signal(unsigned char number){
	if(number == 0)
		return;
	while(true){
		for(int count = 0; count < number; count++){
			nrf_gpio_pin_set(13);
			nrf_delay_ms(100);
			nrf_gpio_pin_clear(13);
			nrf_delay_ms(400);
		}
		nrf_delay_ms(1000);
	}
}

void Err::signalWithCode(unsigned char number, unsigned char code){
	if(number == 0)
		return;
	//while(true){
		for(int count = 0; count < number; count++){
			nrf_gpio_pin_set(13);
			nrf_delay_ms(100);
			nrf_gpio_pin_clear(13);
			nrf_delay_ms(400);
		}
		nrf_delay_ms(500);
		nrf_gpio_pin_set(13);
		nrf_delay_ms(500);
		nrf_gpio_pin_clear(13);
		nrf_delay_ms(500);
		for(int count = 0; count < code; count++){
			nrf_gpio_pin_set(13);
			nrf_delay_ms(100);
			nrf_gpio_pin_clear(13);
			nrf_delay_ms(400);
		}
		nrf_delay_ms(1000);
	//}
}
