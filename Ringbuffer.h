/*
 * Ringbuffer.h
 *
 *  Created on: 15.12.2018
 *      Author: Leon
 */

#ifndef PROJECT_MEASUREMENT_PROTOTYPE_RINGBUFFER_H_
#define PROJECT_MEASUREMENT_PROTOTYPE_RINGBUFFER_H_

class Ringbuffer {
public:
	Ringbuffer(unsigned char size);
	~Ringbuffer();

	void write(unsigned char value);
	unsigned char read();

	unsigned char isAvailable();
	void flush();

private:
	unsigned char* buffer;
	unsigned char bufferSize;
	unsigned char readIndex, writeIndex;
	unsigned char available;
};

#endif /* PROJECT_MEASUREMENT_PROTOTYPE_RINGBUFFER_H_ */
