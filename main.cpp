//OWN CODE

#define NRF_BLE_SCAN_BUFFER 64
#include "nrf.h"
#include "nrf_nvic.h"
//#include "../../ble/nrf_ble_scan/nrf_ble_scan.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_uart.h"
#include "nrf_log.h"

#include "sdk_config.h"
#include <new>
#include "UARTHandler.h"
#include "UltrasonicModule.h"
#include "BLEHandler.h"
#include "ConfigCalculator.h"

#include "ble_hci.h"
#include "ble_gap.h"
#include "ble_err.h"
#include "ble_srv_common.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "BNO055.h"
#include "nrfx_twi.h"
#include "GPIO.h"
#include "Drone.h"
#include "BLEMessageType.h"
#include "ControlMultiplexer.h"
#include "HF.h"
#include "I2CControl.h"
#include "BMP388.h"
#include "VL53L0X.h"
#include "testfunctions.h"
#include "PIDControl.h"

#include "app_timer.h"
#include "nrf_drv_clock.h"

#define BLE_MESSAGE_TYPE 0x04
#define BLE_DRONE_ID 0x02

#define UPDATE_MS 10

#define SLEEP_TICK_MS 1
#define SLEEP_PAIRCYCLE_CS 16

#define TRIGGER_INTERVAL_CS 10

void dummyMethod(){;}
void* currentMethod = (void*)dummyMethod;

Drone* drone;
UARTHandler* uart0;
UARTHandler* uart1;
UltrasonicModule* usm0;
UltrasonicModule* usm1;
ControlMultiplexer* control;
I2CControl* i2c;

BNO055* bno;
BMP388* bmp;
VL53L0X* vl53;

//parts of these variables might be transferred to some kind of slave/master toolbox class. We will see how it works out
bool* ackList;
unsigned char* droneProtoList;
unsigned char numOfProtoListDrones = 0;
unsigned int timeCounter = 0;
bool ackSend = false;
unsigned char ackCounter = 0;

unsigned char listenTimeout = 0;
bool triggered = false;
short shoutTimer = 0;
bool isTriggering = false;

bool enterPressed = false;

bool isMaster;

//function prototypes
void masterInitMethod();
void BLEInitMaster();
void BLEMasterDroneList();
void measurementCycleMethod();
void BLETriggerTXCallback();
void BLEMeasurementCycle();

APP_TIMER_DEF(timerHandle);


void updateEnterButton(){
	static bool last = false;
	static bool now = false;
	now = !nrf_gpio_pin_read(BUTTON_ENTER);

	if(!now && last){
		enterPressed = true;
	}else{
		enterPressed = false;
	}
	last = now;

}


static void lfclk_request(void)
{
    ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);
    nrf_drv_clock_lfclk_request(NULL);
}


//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//-------------------------------------------------------- main states -----------------------------------------------
/*
 * The following code block contains all states for the main state machine. This mchine runs its current state periodially to update
 * various different functionalities, such as positional measurement logic, PID controls, gathering of sensor data, and many more
 *
 * the current state is saved in a function pointer called currentMethod.
 * Redefining the pointer changes the state the machine is in.
 *
 * Example of changing the state:
 * currentMethod = (void*) newMethod;
 */


/**
 * initial drone state after initialisation. The program waits for a designated master to send a call request.
 * Once a request is received, the drone answers in its timeslot according to its hardware id.
 * If the "enter button is pressed, the drone is switched into master mode, periodically sending out call requests.
 */
void slaveInitMethod(){
	if(timeCounter == 0){
		nrf_gpio_pin_toggle(LED0);
		timeCounter = 50;

		if (ackSend) {
			drone->sendACK();
			ackSend = false;
		}
	}


	if(enterPressed){
		BLEHandler::setCBRFunction((void*)BLEInitMaster);
		currentMethod = (void*)masterInitMethod;
		nrf_gpio_pin_set(LED1);
		nrf_gpio_pin_clear(LED2);
		isMaster = true;
	}
}

/**
 * state the drone is in after a drone list command was received and processed.
 */
void slaveReadyMethod(){
	static bool isSet = false;
	if(!isSet){
		nrf_gpio_pin_set(LED0);
		isSet = true;
	}

	if (ackSend) {
		drone->sendACK();
		ackSend = false;
	}
}


/**
 * temporary method used to stop program execution once the drone list was broadcasted.
 */
void masterReadyTempMethod(){
	static bool isSet = false;

	if(!isSet){
		nrf_gpio_pin_set(LED0);
		isSet = true;
	}
}

/**
 * This method is used by the master to safely broadcast the list of found drones to all participants.
 * For this it creates an acknowledgment list and periodically sends out the drone list until all
 * participants have answered.
 */
void trigonometryTestMethod();
void trigonometryDemonstratorMethod();
void BLEMeasurementCycleTrigonometryTest();

void masterDroneListMethod(){

	//create an acknowledgment list and reset it the first time you enter the function
	if(ackList == nullptr){
		ackList = new bool[numOfProtoListDrones];
		for(int count = 0; count < numOfProtoListDrones; count++){
			ackList[count] = false;
		}
		ackCounter = 0;
	}

	if(timeCounter == 0){
		if(ackCounter >= numOfProtoListDrones){
			//move over to measurement cycle mode
			//currentMethod = (void*)masterReadyTempMethod;
			//deactivating the master drone from flying
			//currentMethod = (void*)measurementCycleMethod;
			currentMethod = ((void*)trigonometryDemonstratorMethod);
			BLEHandler::setCBTFunction((void*)BLETriggerTXCallback);
			BLEHandler::setCBRFunction((void*)BLEMeasurementCycleTrigonometryTest);
		}else{
			timeCounter = (numOfProtoListDrones+1)*SLEEP_TICK_MS;
			drone->sendDroneList();
		}
	}
}

/**
 * this method is the master initialisation routine for the pairing cycle.
 * it periodically sends out call requests to all potential swarm participants un the area.
 * Incoming answers are taken care of by the respective BLE state, BLEInitMaster().
 */
void masterInitMethod(){
	static unsigned char iterationCounter = 0;

	if(timeCounter == 0){
		//after five calling rounds, the pairing mode is abandoned and the drone list is sent
		if(iterationCounter > 5){
			currentMethod = (void*)masterDroneListMethod;
			BLEHandler::setCBRFunction((void*)BLEMasterDroneList);
			drone->setDroneList(droneProtoList, numOfProtoListDrones);
		}else{
			timeCounter = SLEEP_PAIRCYCLE_CS;
			drone->sendCall();
			iterationCounter++;
		}
	}
}

/**
 * This is the main measurement state routine.
 * Both slaves and master use this method.
 * It manages the timing of the slot in which the drone is allowed to send out ultrasonic blasts
 * and processes the result of passive ranging.
 * This will be expanded to include PID controlled autonomous flight and other sensor data evaluation.
 */
void measurementCycleMethod(){
	if(listenTimeout > 0){
		listenTimeout--;
	}else if(triggered){
		//usm->setRegisterValue(0x40, 0x80);
		//usm->getDataDump(false,true);
		drone->calculateDistanceAndAngleResult(usm0->getUSTravelTime(),usm1->getUSTravelTime(),10); //TODO: implement timing in original measurement cycle method
		triggered = false;
	}

	if(shoutTimer > 0){
		shoutTimer--;
	}else{
		isTriggering = true;
		drone->sendTrigger(isMaster);
		shoutTimer = TRIGGER_INTERVAL_CS * drone->getNumberOfDronesInSwarm()/*+1 add this when master info messages are implemented TODO!!!*/;
	}
}

/**
 * demonstration method derived from the measurement cycle method used to showcase all softwaremodules working
 */
void demonstratorCycleMethod() {

	static int switchCounter = 0;
	static float xTarget = -0.75f;

	static float target = -1;
	static float channelFloatValue = 0;
	static PIDControl<float> heightPID(0.2f,0.025f,7.5f, 0.1f, 0.15f);
	static PIDControl<float> rollPID(1.0f ,0.05f,3.0f, 0.1f, 0.15f);
	static PIDControl<float> pitchPID(0.5f ,0.025f,3.0f, 0.1f, 0.15f);
	static PIDControl<float> speedXPID(0.8f ,0.01f,0.5f, 0.1f, 0.15f);
	static PIDControl<float> speedYPID(0.8f ,0.01f,0.5f, 0.1f, 0.15f);
	static float steerX = 0.0f;
	static float steerY = 0.0f;
	static unsigned int timeSinceLastDistanceMeasurement = 0;

	timeSinceLastDistanceMeasurement++;

	if (listenTimeout > 0) {
		listenTimeout--;
	} else if (triggered) {
		drone->calculateDistanceAndAngleResult(usm0->getUSTravelTime(),	usm1->getUSTravelTime(), timeSinceLastDistanceMeasurement);
		timeSinceLastDistanceMeasurement = 0;
		triggered = false;
	}

	if (shoutTimer > 0) {
		shoutTimer--;
	} else {
		isTriggering = true;
		drone->sendTrigger(isMaster);
		shoutTimer = TRIGGER_INTERVAL_CS * drone->getNumberOfDronesInSwarm()/*+1 add this when master info messages are implemented TODO!!!*/;
	}

	/**
	 * VL53
	 */
	if (timeCounter == 0) {
		timeCounter = 10;

		//Deactivating base drone
		if(drone->getIndex() == 0)
			control->setChannel(4,-1.0f);

		/*switchCounter = (switchCounter+1)%160;
		if(switchCounter == 0){
			xTarget = -0.75f;
		}else if (switchCounter == 80){
			xTarget = -1.25f;
		}*/


		vl53->update();
		drone->updateVirtualDrone(steerX, steerY, 0.1f);

		if (control->getChannel(5) > -0.1f) {
			if (target < 0) {
				//target = vl53->getHeight();
				//channelFloatValue = control->getChannel(2);

				target = 0.72f;
				channelFloatValue = 0.05f;

				heightPID.reset();
				rollPID.reset();
				pitchPID.reset();
				speedXPID.reset();
				speedYPID.reset();
			}

			//BLEHandler::sendDebugFloat(vl53->getHeight());
			nrf_gpio_pin_set(LED3);
			heightPID.update(vl53->getHeight(), target, 0.1f);
			float tempChannelValue = HF::clamp(heightPID.getPIDValue(),-0.2f, 0.2f);
			control->setChannel(2, tempChannelValue + channelFloatValue);




			/**
			 * positionPID
			 */
			rollPID.update(drone->getXPos() , xTarget,0.1f);
			pitchPID.update(drone->getYPos() , 0.0f,0.1f);
			speedXPID.update(drone->getXSpd(),rollPID.getPIDValue(),0.1f);
			speedYPID.update(drone->getYSpd(),pitchPID.getPIDValue(),0.1f);
			steerX = HF::clamp(speedXPID.getPIDValue(),-0.25f,0.25f);
			control->setChannel(0, steerX);
			if(control->getChannel(5) > 0.5f){
				steerY = HF::clamp(speedYPID.getPIDValue(),-0.25f,0.25f);
				control->setChannel(1, steerY);
			}else{
				steerY = 0.0f;
				control->freeChannel(1);
			}

		} else {
			control->freeChannel(0);
			control->freeChannel(1);
			control->freeChannel(2);
			steerX = 0.0f;
			steerY = 0.0f;
			drone->resetVirtualDrone();
			target = -1;
			nrf_gpio_pin_clear(LED3);
		}

	}
}


void trigonometryTestMethod() {

	if (listenTimeout > 0) {
		listenTimeout--;
	} else if (triggered) {
		drone->calculateTrigonometryPosResult(usm0->getUSTravelTime());
		triggered = false;
	}

	if (shoutTimer > 0) {
		shoutTimer--;
	} else {
		isTriggering = true;
		shoutTimer = TRIGGER_INTERVAL_CS * 3;//drone->getNumberOfDronesInSwarm()/*+1 add this when master info messages are implemented TODO!!!*/;
		if(drone->getIndex() < 2)
			drone->sendTrigger(isMaster);
	}


}


void trigonometryDemonstratorMethod(){
	static int switchCounter = 0;
	static float xTarget = -0.75f;

	static float target = -1;
	static float channelFloatValue = 0;
	static PIDControl<float> heightPID(0.10f,0.035f,7.0f, 0.1f, 0.15f);
	static PIDControl<float> rollPID(0.5f ,0.075f,15.0f, 0.1f, 0.15f);
	static PIDControl<float> pitchPID(0.5f ,0.075f,15.0f, 0.1f, 0.15f);
	static PIDControl<float> speedXPID(0.5f ,0.0f,0.5f, 0.1f, 0.15f);
	static PIDControl<float> speedYPID(0.5f ,0.0f,0.5f, 0.1f, 0.15f);
	static float smoothHeight = 0.0f;
	static float steerX = 0.0f;
	static float steerY = 0.0f;
	static unsigned int timeSinceLastDistanceMeasurement = 0;

	timeSinceLastDistanceMeasurement++;

	if (listenTimeout > 0) {
		listenTimeout--;
	} else if (triggered) {
		drone->calculateTrigonometryPosResult(usm0->getUSTravelTime(), timeSinceLastDistanceMeasurement);
		timeSinceLastDistanceMeasurement = 0;
		triggered = false;
	}

	if (shoutTimer > 0) {
		shoutTimer--;
	} else {
		isTriggering = true;
		drone->sendTrigger(isMaster);
		shoutTimer = TRIGGER_INTERVAL_CS * drone->getNumberOfDronesInSwarm()/*+1 add this when master info messages are implemented TODO!!!*/;
	}

	/**
	 * VL53
	 */
	if (timeCounter == 0) {
		timeCounter = 10;

		//Deactivating base drones
		if(drone->getIndex() <= 1)
			control->setChannel(4,-1.0f);

		/*switchCounter = (switchCounter+1)%160;
		if(switchCounter == 0){
			xTarget = -0.75f;
		}else if (switchCounter == 80){
			xTarget = -1.25f;
		}*/


		vl53->update();
		if(vl53->getHeight() < 1.5f){
			smoothHeight= 0.3f * smoothHeight + 0.7f * HF::clamp(vl53->getHeight(),0.0f,1.5f);
		}
		drone->setZPos(smoothHeight);
		drone->updateVirtualDrone(steerX, steerY, 0.1f);

		if (control->getChannel(5) > -0.1f) {
			if (target < 0) {
				//target = vl53->getHeight();
				//channelFloatValue = control->getChannel(2);

				target = 0.6f;
				channelFloatValue = -0.3f;

				heightPID.reset();
				rollPID.reset();
				pitchPID.reset();
				speedXPID.reset();
				speedYPID.reset();
			}

			//BLEHandler::sendDebugFloat(vl53->getHeight());
			nrf_gpio_pin_set(LED3);
			heightPID.update(smoothHeight, target, 0.1f);
			float tempChannelValue = HF::clamp(heightPID.getPIDValue(),-0.2f, 0.2f);
			control->setChannel(2, tempChannelValue + channelFloatValue);




			/**
			 * positionPID
			 */
			rollPID.update(drone->getXPos() , 0.35f,0.1f);
			pitchPID.update(drone->getYPos() , 0.75f,0.1f);
			speedXPID.update(drone->getXSpd(),rollPID.getPIDValue(),0.1f);
			speedYPID.update(drone->getYSpd(),pitchPID.getPIDValue(),0.1f);
			steerX = HF::clamp(speedXPID.getPIDValue(),-0.25f,0.25f);
			control->setChannel(0, steerX);
			if(control->getChannel(5) > 0.5f){
				steerY = HF::clamp(speedYPID.getPIDValue(),-0.25f,0.25f);
				control->setChannel(1, steerY);
			}else{
				steerY = 0.0f;
				control->freeChannel(1);
			}

		} else {
			control->freeChannel(0);
			control->freeChannel(1);
			control->freeChannel(2);
			steerX = 0.0f;
			steerY = 0.0f;
			drone->resetVirtualDrone();
			target = -1;
			nrf_gpio_pin_clear(LED3);
		}

	}
}


//---------------------------------------------------------- END ----------------------------------------------------
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX











//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//-------------------------------------------------------- BLE states -----------------------------------------------
/*
 * The following code block contains the high level logic regarding incoming and outgoing BLE communication traffic.
 * These functions are plugged into the BLEHandler TX/RX callbacks as function pointers and are executed accordingly.
 *
 * Changing the pointer will result in the BLE state machine "changing state".
 * This can be done with the following code snippets:
 * BLEHandler::setCBTFunction((void*)TXCallbackFunction);
 * BLEHandler::setCBRFunction((void*)RXCallbackFunction);
 */


/**
 * This TX state is used to send a scream command to the ultrasonic module once the trigger is sent
 */
void BLETriggerTXCallback(){
	//TODO:Reenable after trigonometry test.
	/*static unsigned char usIndex = 0;


	if(isTriggering){*/
	isTriggering = false;
		/*switch(usIndex){
		case 0:*/
			usm0->scream();
			/*break;
		case 1:
		default:
			usm1->scream();
			break;
		}
		usIndex = (usIndex+1)%2;*/
	//}
}

/**
 * The main measurement cycle BLE state. Its main function is to start the timing process once a trigger is received.
 */
void BLEMeasurementCycle(){
	unsigned char* msg = BLEHandler::getMessageData();
	if(msg[BLE_MESSAGE_TYPE] == BLEMessageType::TRIGGER){
		//usm->measure(false);
		usm0->listenAll(); //this does not only trigger usm0 but both modules to listen to incoming us pings
		triggered = true;
		listenTimeout = 5;
		//temporarily store the difference in index of the message sender to the own index for the shout timer calculation
		shoutTimer = /*TRIGGER_INTERVAL_MS * */(drone->getIndex()-msg[BLE_DRONE_ID]);
		//if the sender drone index is higher, en entire wraparound wait cycle is calculated
		if(shoutTimer < 0){
			shoutTimer += drone->getNumberOfDronesInSwarm()/*+1 add this when master info messages are implemented TODO!!!*/;
		}
		//multiply the result with the trigger interval constant
		shoutTimer *= TRIGGER_INTERVAL_CS;
	}
}

/**
 * TRIGONOMY TEST FUNCTION
 * The main measurement cycle BLE state. Its main function is to start the timing process once a trigger is received.
 */
void BLEMeasurementCycleTrigonometryTest(){
	unsigned char* msg = BLEHandler::getMessageData();
	if(msg[BLE_MESSAGE_TYPE] == BLEMessageType::TRIGGER){
		//usm->measure(false);
		usm0->listenAll(); //this does not only trigger usm0 but both modules to listen to incoming us pings
		triggered = true;
		listenTimeout = 5;
		//setting id of drone that the measurement ist taken to.
		drone->setReferenceID(msg[BLE_DRONE_ID]);
		//temporarily store the difference in index of the message sender to the own index for the shout timer calculation
		shoutTimer = /*TRIGGER_INTERVAL_MS * */(drone->getIndex()-msg[BLE_DRONE_ID]);
		//if the sender drone index is higher, an entire wraparound wait cycle is calculated
		if(shoutTimer < 0){
			shoutTimer += drone->getNumberOfDronesInSwarm()/*+1 add this when master info messages are implemented TODO!!!*/;
		}
		//multiply the result with the trigger interval constant
		shoutTimer *= TRIGGER_INTERVAL_CS;
	}
}

/**
 * Initial drone BLE state. This state waits for incoming call requests as well as identification lists and a first trigger, which changes the state to the measurement cycle.
 */
void BLEInitSlave(){
	unsigned char* msg = BLEHandler::getMessageData();

	switch(msg[BLE_MESSAGE_TYPE]){
	case BLEMessageType::CALL:
		ackSend = true;
		timeCounter = (SLEEP_TICK_MS*(drone->getIndex()+1));
		break;
	case BLEMessageType::DRONE_LIST:
		drone->setDroneList(msg);
		ackSend = true;
		timeCounter = (SLEEP_TICK_MS*(drone->getIndex()+1));
		currentMethod = (void*) slaveReadyMethod;
		break;
	case BLEMessageType::TRIGGER:
		shoutTimer = /*TRIGGER_INTERVAL_MS * */(drone->getIndex()-msg[BLE_DRONE_ID]);
		//if the sender drone index is higher, en entire wraparound wait cycle is calculated
		if(shoutTimer < 0){
			shoutTimer += drone->getNumberOfDronesInSwarm()/*+1 add this when master info messages are implemented TODO!!!*/;
		}
		//multiply the result with the trigger interval constant
		shoutTimer *= TRIGGER_INTERVAL_CS;

		if(drone->isReady()){
			//currentMethod = (void*)measurementCycleMethod;
			//currentMethod = (void*)demonstratorCycleMethod;
			currentMethod = (void*)trigonometryDemonstratorMethod;
			//TODO: testing the direction measurement control fidelity, remove if done!
			//currentMethod = (void*)flying90DegTestMethod;
			BLEHandler::setCBTFunction((void*)BLETriggerTXCallback);
			//BLEHandler::setCBRFunction((void*)BLEMeasurementCycle);
			BLEHandler::setCBRFunction((void*)BLEMeasurementCycleTrigonometryTest);
		}
		break;
	default:
		break;
	};
}

/**
 * master initialisation BLE state. Once a drone is designated to be the master, it periodically calls for swarm participants and listens for acknowledgments.
 * This function is responsible for the receiving part.
 * Found drones are added to a proto drone list, which is later used to collect acknowledgments on other important messages.
 */
void BLEInitMaster(){
	unsigned char* msg = BLEHandler::getMessageData();
	if(msg[BLE_MESSAGE_TYPE] == BLEMessageType::ACK){
		bool found = false;
		for(int count = 0; count < numOfProtoListDrones; count++){
			if(droneProtoList[count] == msg[BLE_DRONE_ID]){
				found = true;
				break;
			}
		}
		if(!found){
			droneProtoList[numOfProtoListDrones] = msg[BLE_DRONE_ID];
			numOfProtoListDrones++;
		}
	}
}

/**
 * this BLE state collects acknowledgments from other participants in the swarm.
 */
void BLEMasterDroneList(){
	unsigned char* msg = BLEHandler::getMessageData();
	if(msg[BLE_MESSAGE_TYPE] == BLEMessageType::ACK && ackList[msg[BLE_DRONE_ID]-1] == false){
		ackList[msg[BLE_DRONE_ID]] = true;
		ackCounter++;
	}
}
//---------------------------------------------------------- END ----------------------------------------------------
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

static void update(void* p_context) {
	static bool running = false;

	if(!running){
		if (timeCounter > 0) {
			timeCounter--;
		}

		running = true;
		updateEnterButton();
		((void (*)(void)) currentMethod)();
		running = false;
	}
}


/**
 * main entry point
 */
int main(void) {
	NRF_LOG_INIT(NULL);
	NRF_LOG_DEFAULT_BACKENDS_INIT();
	lfclk_request();
	app_timer_init();

	//sets all basic IO pins to the respecting configuration
	GPIO::initBase();
	NRF_LOG_INFO("bno init");
	nrf_delay_ms(3000);


	//-----------------------------------------------------I2C Interface Test-----------------------------------------------------
	i2c = new I2CControl();
	//bno = new BNO055(i2c);
	//NRF_LOG_INFO("bno init");
	//bno->init();

		/*VecContainer testVector;


		for(int count = 0; count < 1000; count++){
			nrf_delay_ms(1000);

			testVector = bno->getVector();

			//bno055_convert_float_euler_hpr_deg(&eulerData);
			NRF_LOG_INFO("chip ID -> %d", bno->getChipID());
			NRF_LOG_INFO("mode -> %d", bno->read8(0x3d));
			NRF_LOG_INFO("calib -> %d", bno->read8(0x35));
			NRF_LOG_INFO("sys -> %d", bno->read8(0x3a));
			NRF_LOG_INFO("stat -> %d", bno->read8(0x39));
			NRF_LOG_INFO("selftest -> %d", bno->read8(0x36));
			NRF_LOG_INFO("x:" NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(testVector.x));
			NRF_LOG_INFO("y:" NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(testVector.y));
			NRF_LOG_INFO("z:" NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(testVector.z));
		}*/

	//bmp = new BMP388(i2c);
	//bmp->init();

	/*while(true){
		nrf_delay_ms(250);
		bmp->update();
		NRF_LOG_INFO("temperature -> " NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(bmp->getTemperature()));
		NRF_LOG_INFO("pressure -> " NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(bmp->getPressure()));
		/*NRF_LOG_INFO("mode -> %d", bmp->i2c->read8(0x76,0x1b));
		NRF_LOG_INFO("resolution -> %d", bmp->i2c->read8(0x76,0x1c));
		NRF_LOG_INFO("iir -> %d", bmp->i2c->read8(0x76,0x1f));
		NRF_LOG_INFO("error -> %d", bmp->i2c->read8(0x76,0x02));
		NRF_LOG_INFO("chipID -> %d", bmp->i2c->read8(0x76,0x00));
	}*/

	//----------------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------Z-Ranger Test----------------------------------------------------------
	vl53 = new VL53L0X(i2c);
	vl53->init();

	/*while(true){
		nrf_delay_ms(500);
		vl53->update();
		NRF_LOG_INFO("" NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(vl53->getHeight()));
	}*/
	//----------------------------------------------------------------------------------------------------------------------------




	BLEHandler::init(0x00,0x00,0x00);

	droneProtoList = new unsigned char[16];

	drone = new Drone();

	uart0 = new UARTHandler(0,6,8, NRF_UARTE_BAUDRATE_115200);

	BLEHandler::changeID(drone->getID());


	//turns one of the US modules off
	nrf_gpio_pin_clear(US_MODULE0_PWR);
	nrf_delay_ms(1000);
	usm1 = new UltrasonicModule(uart0,0);
	usm1->init();
	usm1->changeID(0x01);

	//turn on the other ultrasonic module
	nrf_gpio_pin_set(US_MODULE0_PWR);
	nrf_delay_ms(1000);
	usm0 = new UltrasonicModule(uart0,0);
	usm0->init();


	//-----------------------------------------UARTE1 Quad Communication ---------------------------------------------------------
	uart1 = new UARTHandler(1,UART1_TX, UART1_RX,NRF_UARTE_BAUDRATE_115200);
	control = new ControlMultiplexer(uart1);
	//----------------------------------------------------------------------------------------------------------------------------




	//all drones are set up to be slaves at the beginning, the switch to a master will be made through the press of the enter button.
	//after pairing is complete, the master drone will become droneID 0 through the use of the droneList command
	BLEHandler::setCBRFunction((void*)BLEInitSlave);
	currentMethod = (void*) slaveInitMethod;
	//currentMethod = (void*) inFlightThresMeasurementTestMethod;
	nrf_gpio_pin_set(LED2);

	NRF_LOG_INFO("init complete");

	//create repeated update task
	app_timer_create(&timerHandle,APP_TIMER_MODE_REPEATED,update);
	app_timer_start(timerHandle, APP_TIMER_TICKS(UPDATE_MS), NULL);

	//main execution loop
	while(true){
		nrf_delay_ms(100000);
		/*static bool running = false;

		nrf_delay_ms(10);
		if(!running){
			if (timeCounter > 0) {
				timeCounter--;
			}

			running = true;
			updateEnterButton();
			((void (*)(void)) currentMethod)();
			running = false;
		}*/
	}

	delete drone;
	delete usm0;
	delete usm1;
	delete uart0;
	delete uart1;
	delete control;
	delete i2c;
	delete bno;
	delete [] droneProtoList;
}
