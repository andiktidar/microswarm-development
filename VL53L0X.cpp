/*
 * VL53L0X.cpp
 *
 *  Created on: 07.03.2019
 *      Author: Leon
 */

#include "VL53L0X.h"
#include "nrf_log.h"
#include "vl53l0x_i2c_platform.h"
#include "I2CControlTransfer.h"

I2CControl* vl53_i2c;

VL53L0X::VL53L0X(I2CControl* i2c)
:lastHeight(0)
{
	vl53_i2c = i2c;
}

VL53L0X::~VL53L0X() {
	// TODO Auto-generated destructor stub
}


void VL53L0X::init(){
	uint32_t err_code = 0;

	    // Initialize.
	    APP_ERROR_CHECK(err_code);

	//    // Initialize Comms
	    vlDevice.I2cDevAddr      =  0x29;

	    VL53L0X_Version_t                   Version;
	    VL53L0X_Version_t                  *pVersion   = &Version;
	    uint8_t deviceRevisionMajor;
	    uint8_t deviceRevisionMinor;

	    uint8_t vhvCalibrationValue;
	    uint8_t phaseCalibrationValue;

	    err_code = VL53L0X_comms_initialise(I2C, vlDevice.comms_speed_khz);

			if(VL53L0X_ERROR_NONE == err_code){

				err_code = VL53L0X_DataInit(&vlDevice);
				if(VL53L0X_ERROR_NONE != err_code) NRF_LOG_ERROR("DataInit: %d\r\n", err_code);

				uint16_t osc_calibrate_val=0;
				err_code = VL53L0X_RdWord(&vlDevice, VL53L0X_REG_OSC_CALIBRATE_VAL,&osc_calibrate_val);
				NRF_LOG_DEBUG("%i\n",osc_calibrate_val);

				err_code = VL53L0X_StaticInit(&vlDevice);
				if(VL53L0X_ERROR_NONE != err_code) NRF_LOG_ERROR("StaticInit: %d\r\n", err_code);

				uint32_t refSpadCount;
				uint8_t isApertureSpads;

				VL53L0X_PerformRefSpadManagement(&vlDevice, &refSpadCount, &isApertureSpads);
				if(VL53L0X_ERROR_NONE != err_code) {
					NRF_LOG_ERROR("SpadCal: %d\r\n", err_code)
				} else {
					NRF_LOG_DEBUG("refSpadCount: %d\r\nisApertureSpads: %d\r\n", refSpadCount,isApertureSpads)
				}

				err_code = VL53L0X_PerformRefCalibration(&vlDevice, &vhvCalibrationValue, &phaseCalibrationValue);
				NRF_LOG_DEBUG("Calibration Values:\r\n VHV:%d phaseCal:%d\r\n", vhvCalibrationValue, phaseCalibrationValue);

				err_code = VL53L0X_GetVersion(pVersion);
				NRF_LOG_INFO("VL53L0X API Version: %d.%d.%d (revision %d)\r\n", pVersion->major, pVersion->minor ,pVersion->build, pVersion->revision);

				err_code = VL53L0X_GetProductRevision(&vlDevice, &deviceRevisionMajor, &deviceRevisionMinor);

				if(VL53L0X_ERROR_NONE == err_code){
					NRF_LOG_INFO("VL53L0X product revision: %d.%d\r\n", deviceRevisionMajor, deviceRevisionMinor);
				} else {
					NRF_LOG_ERROR("Version Retrieval Failed.  ");
				}

				VL53L0X_SetDeviceMode(&vlDevice, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);

			if (err_code == VL53L0X_ERROR_NONE) {
				err_code = VL53L0X_SetLimitCheckValue(&vlDevice,
						VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE,
						(FixPoint1616_t) (0.25 * 65536));
			}

			if (err_code == VL53L0X_ERROR_NONE) {
				err_code = VL53L0X_SetLimitCheckValue(&vlDevice,
						VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE,
						(FixPoint1616_t) (18 * 65536));
			}

			if (err_code == VL53L0X_ERROR_NONE) {
				err_code = VL53L0X_SetMeasurementTimingBudgetMicroSeconds(&vlDevice,
						200000);
			}


	    	VL53L0X_StartMeasurement(&vlDevice);

	    } else {
	    	NRF_LOG_ERROR("Data Init Failed\r\n");
	    }
}

void VL53L0X::update(){

	VL53L0X_GetRangingMeasurementData(&vlDevice, &rangingData);
	VL53L0X_ClearInterruptMask(&vlDevice, 0);

	lastHeight = rangingData.RangeMilliMeter * 0.001f;
}

float VL53L0X::getHeight(){
	return lastHeight;
}
