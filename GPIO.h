/*
 * ControllerConfiguration.h
 *
 *  Created on: 17.01.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_GPIO_H_
#define MICROSWARM_V1_GPIO_H_

#define BUTTON_ENTER 37
#define BUTTON_BACK 38
#define BUTTON_PLUS 39
#define BUTTON_MINUS 40
/*#define BUTTON_ENTER 25
#define BUTTON_BACK 24
#define BUTTON_PLUS 12
#define BUTTON_MINUS 11*/

#define LED0 13
#define LED1 12
#define LED2 15
#define LED3 16

#define UART0_TX 6
#define UART0_RX 8
#define UART1_TX 24
#define UART1_RX 23

#define DIP0 0
#define DIP1 1
#define DIP2 2
#define DIP3 3
#define DIP4 4
#define DIP5 5
/*#define DIP0 33
#define DIP1 34
#define DIP2 35
#define DIP3 37
#define DIP4 36
#define DIP5 38*/

#define I2CSDA 26
#define I2CSCL 27

#define US_MODULE0_PWR 20
#define US_MODULE1_PWR 19

class GPIO {
public:

	static void initBase();
	static bool isPressed(unsigned int pin);

};

#endif /* MICROSWARM_V1_GPIO_H_ */
