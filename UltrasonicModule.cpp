/*
 * UltrasonicModule.cpp
 *
 *  Created on: 29.11.2018
 *      Author: Leon
 */

#include "UltrasonicModule.h"
#include "nrf_drv_uart.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_delay.h"
#include "ConfigCalculator.h"
#include "BLEHandler.h"

#define ANS_LEN_READREG 3
#define CMD_LEN_READREG 4

#define ANS_LEN_SETREG 2
#define CMD_LEN_SETREG 5

#define CMD_READREG 0x09
#define CMD_SETREG 0x0A

#define STARTBYTE 0x55

UltrasonicModule::UltrasonicModule(UARTHandler* uart, unsigned char id)
/*:uartHandle(30,31,NRF_UART_BAUDRATE_19200)*/
{
	uartHandle = uart;
	moduleID = id;
	moduleIDEncoded = (id << 5);
}

void UltrasonicModule::init() {
	memset(answerBuffer, 0, sizeof(unsigned char) * BUFFERSIZE);
	memset(commandBuffer, 0, sizeof(unsigned char) * BUFFERSIZE);
	commandBuffer[0] = STARTBYTE;

	setThresholdConfig();
	//number of cycles

	//set record time
	setRegisterValue(0x22, ConfigCalculator::calcRecordTimeByte(12));

	setRegisterValue(0x14,0xff);
	setRegisterValue(0x17,0x00);
	setRegisterValue(0x1b,0x00);
	setRegisterValue(0x21,0xff);
	setRegisterValue(0x26,0x80);
	setRegisterValue(0x27,0xf8);
	setRegisterValue(0x2a,0x00);
	setRegisterValue(0x21,0xff);



	/*setRegisterValue(0x24, 0xdf);
	setRegisterValue(0x25, 0xff);
	setRegisterValue(0x20, 0x47);
	setRegisterValue(0x27, 0x04);
	setRegisterValue(0x2a, 0x12);*/
}

UltrasonicModule::~UltrasonicModule() {
	// TODO Auto-generated destructor stub
}

bool UltrasonicModule::setRegisterValue(unsigned char regAdr,
		unsigned char regValue, unsigned char recheck) {

	//if the frequency byte is about to be set, reencode the modules id in it
	if(regAdr == 0x1f){
		regValue += moduleIDEncoded;
	}

	if(!checkRegisterValue(regAdr, regValue)){
		do{
			//NRF_LOG_INFO("setting right value....");
			commandBuffer[1] = CMD_SETREG | moduleIDEncoded;
			commandBuffer[2] = regAdr;
			commandBuffer[3] = regValue;
			commandBuffer[4] = calculateCommandChecksum(CMD_LEN_SETREG);

			NRF_LOG_INFO("setRegisterValue command:");
			NRF_LOG_HEXDUMP_INFO(commandBuffer, CMD_LEN_SETREG);
			uartHandle->write(commandBuffer, CMD_LEN_SETREG);

			nrf_delay_ms(10);
		}while (recheck && !checkRegisterValue(regAdr, regValue));
	}
	//NRF_LOG_INFO("already set to right value");
	return true;
}

unsigned char UltrasonicModule::getRegisterValue(unsigned char regAdr) {
	commandBuffer[1] = CMD_READREG | moduleIDEncoded;
	commandBuffer[2] = regAdr;
	commandBuffer[3] = calculateCommandChecksum(CMD_LEN_READREG);

	//NRF_LOG_INFO("checkRegisterValue command:");
	//NRF_LOG_HEXDUMP_INFO(commandBuffer, CMD_LEN_READREG);
	do {
		uartHandle->write(commandBuffer, CMD_LEN_READREG);
	} while (uartHandle->read(answerBuffer, ANS_LEN_READREG)); //returns 1 if answer not received -> redo the value fetch

	return answerBuffer[1];
}

bool UltrasonicModule::checkRegisterValue(unsigned char regAdr,
		unsigned char expectedValue) {

	commandBuffer[1] = CMD_READREG | moduleIDEncoded;
	commandBuffer[2] = regAdr;
	commandBuffer[3] = calculateCommandChecksum(CMD_LEN_READREG);

	//NRF_LOG_INFO("checkRegisterValue command:");
	//NRF_LOG_HEXDUMP_INFO(commandBuffer, CMD_LEN_READREG);
	do {
		uartHandle->write(commandBuffer, CMD_LEN_READREG);
	} while (uartHandle->read(answerBuffer, ANS_LEN_READREG)); //returns 1 if answer not received -> redo the value fetch

	if (answerBuffer[0] != 0x40) {
		NRF_LOG_INFO("us chip error code:");
		NRF_LOG_HEXDUMP_INFO(answerBuffer, 4);
	}
	//NRF_LOG_INFO("checkRegisterValue reponse:");
	//NRF_LOG_HEXDUMP_INFO(answerBuffer, ANS_LEN_READREG);
	if (answerBuffer[1] == expectedValue) {
		return true;
	}
	NRF_LOG_INFO("%d <-> %d", answerBuffer[1],expectedValue);
	return false;
}

unsigned char UltrasonicModule::calculateCommandChecksum(unsigned char length) {
	unsigned char result = 0xff;
	unsigned char temp = result;
	for (int count = 1; count < length - 1; count++) {
		result -= commandBuffer[count];
		if (temp < result)
			result -= 0x01;
		temp = result;
	}
	return result;
}

void UltrasonicModule::testCommandAndAnswer(unsigned char* cmd,
		unsigned char length, unsigned char expectedAnswerLength) {
	for (int count = 0; count < length; count++) {
		commandBuffer[count + 1] = cmd[count];
	}
	commandBuffer[1] = commandBuffer[1] | moduleIDEncoded;
	commandBuffer[length + 1] = calculateCommandChecksum(length + 2);

	NRF_LOG_INFO("custom command:");
	NRF_LOG_HEXDUMP_INFO(commandBuffer, length + 2);

	do {
		uartHandle->write(commandBuffer, length + 2);

		if (expectedAnswerLength == 0) {
			break;
		}

	} while (uartHandle->read(answerBuffer, expectedAnswerLength));

	if (expectedAnswerLength > 0) {

		NRF_LOG_INFO("PGA460 reponse:");
		NRF_LOG_HEXDUMP_INFO(answerBuffer, expectedAnswerLength);
	} else {
		NRF_LOG_INFO("No expected response");
	}
}

void UltrasonicModule::scream() {

	commandBuffer[1] = 0x01 | moduleIDEncoded;
	commandBuffer[2] = 0x01;
	commandBuffer[3] = calculateCommandChecksum(4);
	//NRF_LOG_INFO("scream command:");
	//NRF_LOG_HEXDUMP_INFO(commandBuffer, 4);

	uartHandle->write(commandBuffer, 4);
}

void UltrasonicModule::listen() {

	commandBuffer[1] = 0x03 | moduleIDEncoded;
	commandBuffer[2] = 0x01;
	commandBuffer[3] = calculateCommandChecksum(4);
	//NRF_LOG_INFO("listen command:");
	//NRF_LOG_HEXDUMP_INFO(commandBuffer, 4);

	uartHandle->write(commandBuffer, 4);
}


void UltrasonicModule::listenAll() {

	commandBuffer[1] = 0x14;
	commandBuffer[2] = 0x01;
	commandBuffer[3] = calculateCommandChecksum(4);
	//NRF_LOG_INFO("listen command:");
	//NRF_LOG_HEXDUMP_INFO(commandBuffer, 4);

	uartHandle->write(commandBuffer, 4);
}

unsigned short UltrasonicModule::getUSTravelTime(bool sendDebugStrength) {
	unsigned short result = 0x0000;

	commandBuffer[1] = 0x05 | moduleIDEncoded;
	//commandBuffer[2] = calculateCommandChecksum(3);
	//NRF_LOG_INFO("traveltime command:");
	//NRF_LOG_HEXDUMP_INFO(commandBuffer, 3);

	uartHandle->write(commandBuffer, 2);
	uartHandle->read(answerBuffer, 6);
	//NRF_LOG_HEXDUMP_INFO(answerBuffer, 5);

	if(sendDebugStrength)
		BLEHandler::sendDebugInt(answerBuffer[4]);

	result = (answerBuffer[1] << 8) + answerBuffer[2];
	return result;
}

void UltrasonicModule::measure(bool send) {

	if (send)
		scream();
	else
		listen();
}

void UltrasonicModule::printDataDump(bool printFull, bool interprete){
	commandBuffer[1] = 0x07 | moduleIDEncoded;
	commandBuffer[2] = calculateCommandChecksum(3);
	NRF_LOG_INFO("dump command:");
	NRF_LOG_HEXDUMP_INFO(commandBuffer, 3);

	uartHandle->write(commandBuffer, 3);
	uartHandle->read(answerBuffer, 130);
	if (printFull) {
		NRF_LOG_INFO("listenCommand answer:");
		NRF_LOG_HEXDUMP_INFO(answerBuffer, 64);
		nrf_delay_ms(50);
		NRF_LOG_HEXDUMP_INFO(answerBuffer + 64, 64);
	}
	if (interprete) {
		unsigned char result[16];
		for (int segs = 0; segs < 16; segs++) {
			result[segs] = 0;
			for (int count = 0; count < 8; count++) {
				if (answerBuffer[segs * 8 + count] > result[segs]) {
					result[segs] = answerBuffer[segs * 8 + count];
				}
			}
		}
		NRF_LOG_HEXDUMP_INFO(result, 16);
	}
}

unsigned char UltrasonicModule::getNoiseLevelFromMeasurement(){

	//grab data dump from module;
	commandBuffer[1] = 0x07 | moduleIDEncoded;
	commandBuffer[2] = calculateCommandChecksum(3);
	NRF_LOG_INFO("dump command:");
	NRF_LOG_HEXDUMP_INFO(commandBuffer, 3);
	uartHandle->write(commandBuffer, 3);
	uartHandle->read(answerBuffer, 130);

	float result = 0.0f;

	//filter out the first 6 bytes since they always are 255 or other high values (for some reason, even if not pinging... O.o)
	for(unsigned char count = 6; count < 128; count++){
		result += (answerBuffer[count]/122.0f);
	}

	return (unsigned char)result;
}

void UltrasonicModule::setThresholdConfig() {

	unsigned char thresholdValues[7] ={
			200,
			100,
			40,
			25,
			16,
			13,
			12
	};



	unsigned char thresholdConfig[32];
	thresholdConfig[0] = 0x57;
	thresholdConfig[1] = 0x99;
	thresholdConfig[2] = 0x99;
	thresholdConfig[3] = 0xff;
	thresholdConfig[4] = 0xff;
	thresholdConfig[5] = 0xff;
	thresholdConfig[6] = (thresholdValues[0] & 0b11111000)+((thresholdValues[1] >> 5) & 0b00000111);
	thresholdConfig[7] = ((thresholdValues[1]<<3) & 0b11000000)+((thresholdValues[2] >> 2) & 0b00111110) + ((thresholdValues[3] >> 7 ) & 0b00000001);
	thresholdConfig[8] = ((thresholdValues[3]<<1) & 0b11110000)+((thresholdValues[4] >> 4) & 0b00001111);
	thresholdConfig[9] = ((thresholdValues[4]<<4) & 0b10000000)+((thresholdValues[5] >> 1) & 0b01111100) + ((thresholdValues[6] >> 6 ) & 0b00000011);
	thresholdConfig[10] = ((thresholdValues[6]<<2) & 0b11100000);
	thresholdConfig[11] = 0x00;
	thresholdConfig[12] = 0x00;
	thresholdConfig[13] = 0x00;
	thresholdConfig[14] = 0x00;
	thresholdConfig[15] = 0x02;

	thresholdConfig[16] = 0x57;
	thresholdConfig[17] = 0x99;
	thresholdConfig[18] = 0x99;
	thresholdConfig[19] = 0xff;
	thresholdConfig[20] = 0xff;
	thresholdConfig[21] = 0xff;
	thresholdConfig[22] = (thresholdValues[0] & 0b11111000)+((thresholdValues[1] >> 5) & 0b00000111);
	thresholdConfig[23] = ((thresholdValues[1]<<3) & 0b11000000)+((thresholdValues[2] >> 2) & 0b00111110) + ((thresholdValues[3] >> 7 ) & 0b00000001);
	thresholdConfig[24] = ((thresholdValues[3]<<1) & 0b11110000)+((thresholdValues[4] >> 4) & 0b00001111);
	thresholdConfig[25] = ((thresholdValues[4]<<4) & 0b10000000)+((thresholdValues[5] >> 1) & 0b01111100) + ((thresholdValues[6] >> 6 ) & 0b00000011);
	thresholdConfig[26] = ((thresholdValues[6]<<2) & 0b11100000);
	thresholdConfig[27] = 0x00;
	thresholdConfig[28] = 0x00;
	thresholdConfig[29] = 0x00;
	thresholdConfig[30] = 0x00;
	thresholdConfig[31] = 0x02;


	/*for (unsigned char count = 16; count < 32; count++) {
		nrf_delay_ms(40);
		//diagCmd[1] = count;
		setRegisterValue(count + 0x5f, thresholdConfig[count], false);
	}*/

	for (unsigned char count = 16; count < 32; count++) {
		nrf_delay_ms(40);
		//diagCmd[1] = count;
		setRegisterValue(count + 0x5f, thresholdConfig[count]);
	}
	//frequency 80k
	//setRegisterValue(0x1C, 0xfa);
	//frequency 40k
	setRegisterValue(0x1C, 0x32);
	//setRegisterValue(0x5f, 0xff);
	//unsigned char diagCmd[2] = {0x09,0x00};
}

void UltrasonicModule::changeID(unsigned char newID) {
	unsigned char regValue = getRegisterValue(0x1f);
	regValue &= 0x1f;
	setRegisterValue(0x1f, regValue+(newID << 5), false);
	moduleID = newID;
	moduleIDEncoded = (newID << 5);
}
