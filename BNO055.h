/*
 * BNOInterface.h
 *
 *  Created on: 18.12.2018
 *      Author: Leon
 */

#ifndef PROJECT_BNO055_TEST_BNO055_H_
#define PROJECT_BNO055_TEST_BNO055_H_

#include "I2CControl.h"

typedef struct {
	float x;
	float y;
	float z;
} VecContainer;

class BNO055 {

public:

	/*s8 BNO055_I2C_bus_read(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt);
	 s8 BNO055_I2C_bus_write(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt);
	 void BNO055_delay_msek(u32 msek);*/

	BNO055(I2CControl* i2CControlHandle);
	~BNO055();
	void init();

	unsigned char getChipID();
	unsigned char read8(const unsigned char adr);
	void readLength(const unsigned char adr, unsigned char* buffer, unsigned char length);
	void write8(const unsigned char adr, unsigned char value);
	VecContainer getVector();
private:

	void reset();
	I2CControl* i2c;
};

#endif /* PROJECT_BNO055_TEST_BNO055_H_ */
