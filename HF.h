/*
 * HF.h
 *
 *  Created on: 08.02.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_HF_H_
#define MICROSWARM_V1_HF_H_

class HF {
public:
	static float clamp(float value, float min, float max);
};

#endif /* MICROSWARM_V1_HF_H_ */
