/*
 * I2CControl.cpp
 *
 *  Created on: 20.02.2019
 *      Author: Leon
 */

#include "I2CControl.h"
#include "Err.h"

I2CControl::I2CControl() {

	twi = NRFX_TWIM_INSTANCE(0);

	twiConfig.frequency          = (nrf_twim_frequency_t)NRFX_TWIM_DEFAULT_CONFIG_FREQUENCY;
	twiConfig.scl                = 27;
	twiConfig.sda                = 26;
	twiConfig.interrupt_priority = NRFX_TWIM_DEFAULT_CONFIG_IRQ_PRIORITY;
	twiConfig.hold_bus_uninit    = NRFX_TWIM_DEFAULT_CONFIG_HOLD_BUS_UNINIT;

	Err::signalWithCode(nrfx_twim_init(&twi, &twiConfig, NULL, NULL), 6);
	//APP_ERROR_CHECK(err_code);
	nrfx_twim_enable(&twi);
}

I2CControl::~I2CControl() {
	// TODO Auto-generated destructor stub
}

unsigned char I2CControl::read8u(const unsigned char chipID, const unsigned char adr) {

	unsigned char result = 0x00;

	nrfx_twim_tx(&twi, chipID, &adr, 1, true);
	nrfx_twim_rx(&twi, chipID, &result, 1);

	return result;
}

char I2CControl::read8(const unsigned char chipID, const unsigned char adr) {

	unsigned char result = 0x00;

	nrfx_twim_tx(&twi, chipID, &adr, 1, true);
	nrfx_twim_rx(&twi, chipID, &result, 1);
	return (char)result;
}

unsigned short I2CControl::read16u(const unsigned char chipID, const unsigned char adr) {

	unsigned short result = 0x0000;
	unsigned char temp[2];

	readLength(chipID,adr,temp,2);

	result = temp[0]+(temp[1]<<8);

	return result;
}

unsigned short I2CControl::read32u(const unsigned char chipID, const unsigned char adr) {

	unsigned int result = 0x00000000;
	unsigned char temp[4];

	readLength(chipID,adr,temp,4);

	result = temp[0]+(temp[1]<<8)+(temp[2]<<16)+(temp[3]<<24);

	return result;
}

short I2CControl::read16(const unsigned char chipID, const unsigned char adr) {
	return (short)read16u(chipID,adr);
}

void I2CControl::readLength(const unsigned char chipID, const unsigned char adr, unsigned char* buffer, unsigned char length) {

	//for(int count = 0; count < length; count++){

		nrfx_twim_tx(&twi, chipID, &adr, 1, true);
		nrfx_twim_rx(&twi, chipID, buffer, length);

		//buffer[count] = read8u(chipID, adr+count);
	//}
}

void I2CControl::writeLength(const unsigned char chipID, const unsigned char adr, unsigned char* buffer, unsigned char length) {

	//for(int count = 0; count < length; count++){

	unsigned char* tempBuf = new unsigned char[length+1];

	tempBuf[0] = adr;
	for(int count = 0; count < length; count++){
		tempBuf[count+1] = buffer[count];
	}

	nrfx_twim_tx(&twi, chipID, tempBuf, length+1, false);

	delete [] tempBuf;
		//buffer[count] = read8u(chipID, adr+count);
	//}
}

void I2CControl::write8(const unsigned char chipID, const unsigned char adr, unsigned char value) {

	unsigned char buffer[2] = {adr,value};

	nrfx_twim_tx(&twi, chipID, buffer, 2, false);
}
