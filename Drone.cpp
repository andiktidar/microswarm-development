/*
 * Drone.cpp
 *
 *  Created on: 17.01.2019
 *      Author: Leon
 */

#include "Drone.h"
#include "DroneStatus.h"
#include "GPIO.h"
#include "BLEHandler.h"
#include "BLEMessageType.h"
#include "nrf_log.h"
#include "math.h"
#include "HF.h"

#define MAX_ANGLE 50.0f
#define WEIGHT 0.172f
#define DOWNFORCE 1.687f

#define MIN_ALLOWED_POS_DEVIATION_X 0.1f
#define MIN_ALLOWED_POS_DEVIATION_Y 0.1f


Drone::Drone():
	droneList(nullptr),
	numOfDrones(0),
	status(DroneStatus::INIT_UNKNOWN),
	posX(0),
	posY(0),
	posZ(0),
	speedX(0),
	speedY(0),
	lastMeasPosX(0),
	lastMeasPosY(0),
	posDevSpanX(0),
	posDevSpanY(0),
	measDeltaTimeX(0.0f),
	measDeltaTimeY(0.0f),
	lastDistance(0),
	lastAngle(0),
	referenceID(0)
{
	id = (GPIO::isPressed(DIP0) ? 0x01:0x00)
			+(GPIO::isPressed(DIP1) ? 0x02:0x00)
			+(GPIO::isPressed(DIP2) ? 0x04:0x00)
			+(GPIO::isPressed(DIP3) ? 0x08:0x00)
			+(GPIO::isPressed(DIP4) ? 0x10:0x00)
			+(GPIO::isPressed(DIP5) ? 0x20:0x00);

	index = id;
	trigDistances[0] = 1.0f;
	trigDistances[1] = 1.0f;
}

Drone::~Drone() {
	delete[] droneList;
}

unsigned char Drone::getID(){
	return id;
}

unsigned char Drone::getIndex(){
	return index;
}

unsigned char Drone::getNumberOfDronesInSwarm(){
	return numOfDrones;
}

void Drone::calculateDistanceResult(unsigned short distUS){
	lastDistance  = distUS * 0.3432f - 84.75f;
	NRF_LOG_INFO("lastDistance: %d,%d", distUS,lastDistance);

	//float distF = distUS * 0.03432f - 7.35f;
	//NRF_LOG_INFO("%d - " NRF_LOG_FLOAT_MARKER, distUS, NRF_LOG_FLOAT(distF));
}

/**
 * calculates the current distance and angle from the current measurement base with the given ultrasonic travel times in useconds
 */
void Drone::calculateDistanceAndAngleResult(unsigned short distUS0, unsigned short distUS1, unsigned int time){
	//lastDistance  = (distUS1+distUS2) * 0.1716f - 84.75f;
	unsigned short distance0 = distUS0 * 0.3432f - 84.75f;
	unsigned short distance1 = distUS1 * 0.3432f - 84.75f;
	measDeltaTimeX += time*0.01f;
	measDeltaTimeY += time*0.01f;

	//NRF_LOG_INFO("%d", (distance1-distance0));
	if(distance0 < 5000 && distance1 < 5000){
		lastDistance = (distance0+distance1)*0.5;

		float distDiff = distance1-distance0;

		if(abs(distDiff) < 98.42f){
			float newAngle = acosf(HF::clamp(distance1-distance0,-98.42f,98.42f)/98.42f)/3.14159f*180.0f;
			lastAngle = 0.6f*generateVirtualAngle()+0.4f*newAngle;



			//include the measurement result in the current virtual drones position
			float measPosX = -sin(lastAngle/180.0f*3.14159f)*lastDistance*0.001f;
			float measPosY = -cos(lastAngle/180.0f*3.14159f)*lastDistance*0.001f;

			integrateMeasuredPos(measPosX, measPosY);


			//BLEHandler::sendDebugFull(measPosX,measPosY,posX,posY,speedX,speedY);
		}

	}
	else if(distance0 < 4000){
		lastDistance = distance0;
	}
	else if(distance1 < 4000){
		lastDistance = distance1;
	}



	//BLEHandler::sendDebugFloat(lastAngle);

	//NRF_LOG_INFO("dist: %d  angle: %d, x: %d, y: %d", lastDistance, (short)lastAngle, (int)(posX*1000.0f), (int)(posY*1000.0f));

}


void Drone::integrateMeasuredPos(float measX, float measY) {
	float devX = abs(measX - lastMeasPosX);
	float devY = abs(measY - lastMeasPosY);

	//if (devX < posDevSpanX + MIN_ALLOWED_POS_DEVIATION_X) {
		posX = 0.3f * posX + 0.7f * measX;
		if (measDeltaTimeX < 1.0f) {
			speedX = 0.5f * speedX + 0.5f * ((measX - lastMeasPosX) * measDeltaTimeX);
		}
		lastMeasPosX = posX;
	/*} else {
		lastMeasPosX = 0.5f * lastMeasPosX + 0.5f * measX;
	}*/
	//if (devY < posDevSpanY + MIN_ALLOWED_POS_DEVIATION_Y) {
		posY = 0.3f * posY + 0.7f * measY;
		if (measDeltaTimeY < 1.0f) {
			speedY = 0.5f * speedY + 0.5f * ((measY - lastMeasPosY) * measDeltaTimeY);
		}
		lastMeasPosY = posY;
	/*} else {
		lastMeasPosY = 0.5f * lastMeasPosY + 0.5f * measY;
	}*/

	measDeltaTimeX = 0.0f;
	measDeltaTimeY = 0.0f;
	posDevSpanX = 0.5f * devX;
	posDevSpanY = 0.5f * devY;
}


//-----------------------------------------------------------------TRIGONOMETRY APPROACH TEST FUNCTIONS--------------------------------------------------------------

void Drone::calculateTrigonometryPosResult(unsigned short distUS1, unsigned int time){
	if(index == 0){
		posX = 0;
		posY = 0;
	}else if(index == 1){
		posX = distUS1 * 0.0003432f - 0.08475f;
		posY = 0;
	}else{

		//third drone currently does not use the information sent by the second one on its position, 0.7f meters are hard-coded
		float refDistance = 0.7f;

		//delta time between two successful measurements
		measDeltaTimeX += time * 0.01f;
		measDeltaTimeY += time * 0.01f;//TODO: this was missing, was it a bug? Added it, but not tested (added 18.06.2019)

		//if the delta time measured is greater than 6.5ms, the measurement is not used, as it is too unreliable
		if(distUS1 < 6500){


				float distance = distUS1 * 0.0003432f - 0.08475f;

				//the two distances to the reference drones are currently stored inside a simple array, float trigDistances[2].
				//The reference id refers to the id of the latest caller and is set in Drone::setReferenceID() during main.cpp:604 -> BLEMeasurementCycleTrigonometryTest()
				trigDistances[referenceID] = sqrtf((distance*distance)-((refDistance-posZ)*(refDistance-posZ)));

				//only update the position if both measurements have been received (a bit sketchy since the refID0-measurement could still have failed)
				if(referenceID == 1){
					float c = refDistance;
					float x = ((trigDistances[0]*trigDistances[0]) + (c*c) - (trigDistances[1]*trigDistances[1])) / (2*c);

					if((x*x) <= (trigDistances[0]*trigDistances[0])){
						float y = sqrtf((trigDistances[0]*trigDistances[0])-(x*x));

						measDeltaTimeY = measDeltaTimeX;
						//TODO: reuse integrateMeasuredPos() instead of integrating it with redundant code.
						//integrateMeasuredPos(x,y);
						posX = x;//0.5f * posX + 0.5f * x;
						posY = y;//0.5f * posY + 0.5f * y;
						speedX = /*0.5f * speedX + 0.5f * */((x - lastMeasPosX) * measDeltaTimeX);
						speedY = /*0.5f * speedY + 0.5f * */((y - lastMeasPosY) * measDeltaTimeY);
						lastMeasPosX = posX;
						lastMeasPosY = posY;
						measDeltaTimeX = 0;
						measDeltaTimeY = 0;
						/*if(index >= 2){
							BLEHandler::sendDebugFull(x,y,0.0f,0.0f,0.0f,0.0f);
						}*/
					}

				}
			}
	}


}

void Drone::setReferenceID(unsigned char rid){
	referenceID = rid%2; //modulo 2 to protect from accidental access to undefined memory later on
}

unsigned char Drone::getReferenceID(){
	return referenceID;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------

void Drone::setDroneList(unsigned char* msg){
	numOfDrones = msg[5];

	droneList = new unsigned char[numOfDrones];

	for(unsigned char count = 0; count < numOfDrones; count++){
		droneList[count] = (msg[6+count]&0xFF);
		if(droneList[count] == id){
			index = count;
			status = DroneStatus::READY;
		}
	}

	BLEHandler::changeID(index);
}

void Drone::setDroneList(unsigned char* protoList, unsigned char numOfProtoDrones){
	//+1 means that we include ourselves in the list from now on
	numOfDrones = numOfProtoDrones+1;

	droneList = new unsigned char[numOfDrones];

	//automatically set the master drone at index 0, but transmit our HW ID to other drones
	droneList[0] = id;
	index = 0;

	for(unsigned char count = 0; count < numOfDrones; count++){
		droneList[count+1] = protoList[count];
	}

	BLEHandler::changeID(index);
	status = DroneStatus::READY;
}

void Drone::sendACK(){
	unsigned char msg [2] = {BLEMessageType::ACK, status};
	BLEHandler::send(&msg[0],2);
}

void Drone::sendCall(){
	unsigned char msg = BLEMessageType::CALL;
	BLEHandler::send(&msg,1);
}

void Drone::sendTrigger(bool master){
	unsigned char* msg = new unsigned char[14];

	short tempPos;


	msg[0] = BLEMessageType::TRIGGER;
	BLEHandler::floatToCharArray(id == 0 ? 0.0f:posX,&msg[1]);
	BLEHandler::floatToCharArray(id <= 1 ? 0.0f:posY,&msg[5]);
	BLEHandler::floatToCharArray(id <= 1 ? 0.5f:posZ,&msg[9]);
	msg[13] = status;

	BLEHandler::send(&msg[0],14);
	delete[] msg;
}

void Drone::sendDroneList(){
	unsigned char* msg = new unsigned char[numOfDrones+2];
	msg[0] = BLEMessageType::DRONE_LIST;
	msg[1] = numOfDrones;

	for(int count = 0; count < numOfDrones; count++){
		msg[count+2] = droneList[count];
	}

	BLEHandler::send(&msg[0],(numOfDrones+2));
	delete[] msg;
}

float Drone::getLastAngle(){
	return lastAngle;
}

float Drone::getLastDistance(){
	return lastDistance;
}

void Drone::updateVirtualDrone(float steerX, float steerY, float deltaTime){
	float steerAngleX = steerX*MAX_ANGLE;
	float steerAngleY = steerY*MAX_ANGLE;

	float accX = ((DOWNFORCE/cos(steerAngleX/180.0f*3.14159f)) * sin(steerAngleX/180.0f*3.14159f)) / WEIGHT;
	float accY = ((DOWNFORCE/cos(steerAngleY/180.0f*3.14159f)) * sin(steerAngleY/180.0f*3.14159f)) / WEIGHT;

	speedX += (accX * deltaTime);
	speedY += (accY * deltaTime);

	speedX -= (speedX * deltaTime * 0.01f);
	speedY -= (speedY * deltaTime * 0.01f);

	posX += (speedX * deltaTime);
	posY += (speedY * deltaTime);

	//NRF_LOG_INFO("%d-%d-%d-%d", (int)(1000.0f*speedX), (int)(1000.0f*speedY), (int)(1000.0f*posX), (int)(1000.0f*posY));
}

float Drone::getXPos(){
	return posX;
}

float Drone::getYPos(){
	return posY;
}

float Drone::getXSpd(){
	return speedX;
}

float Drone::getYSpd(){
	return speedY;
}

void Drone::setZPos(float z){
	posZ = z;
}

void Drone::resetVirtualDrone(){
	speedX = 0.0f;
	speedY = 0.0f;
}

float Drone::generateVirtualAngle(){
	//TODO: This function needs to account for the position of the sending drone

	if(abs(posX) < 0.05f  && abs(posY) < 0.05f)
		return 1.0f;

	return (atan(getYPos()/getXPos())/-3.14159f*180.0f)+90.0f;
}

bool Drone::isReady(){
	return (status == DroneStatus::READY);
}




