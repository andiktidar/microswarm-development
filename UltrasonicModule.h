/*
 * UltrasonicModule.h
 *
 *  Created on: 29.11.2018
 *      Author: Leon
 */

#ifndef PROJECT_MEASUREMENT_PROTOTYPE_ULTRASONICMODULE_H_
#define PROJECT_MEASUREMENT_PROTOTYPE_ULTRASONICMODULE_H_


#include "UARTHandler.h"

#define BUFFERSIZE 130

class UltrasonicModule {
public:
	UltrasonicModule(UARTHandler* uart, unsigned char id);
	~UltrasonicModule();

	void init();
	bool healthCheck();
	void listen();
	void listenAll();
	void scream();
	unsigned short getUSTravelTime(bool sendDebugStrength = false);
	void measure(bool send);
	void printDataDump(bool printFull, bool interprete);
	unsigned char getNoiseLevelFromMeasurement();
	bool setRegisterValue(unsigned char regAdr, unsigned char regValue, unsigned char recheck = true);
	unsigned char getRegisterValue(unsigned char regAdr);
	bool checkRegisterValue(unsigned char regAdr, unsigned char expectedValue);
	void testCommandAndAnswer(unsigned char* cmd, unsigned char length, unsigned char expectedAnswerLength);
	void changeID(unsigned char newID);
	unsigned char answerBuffer[BUFFERSIZE];


private:

	unsigned char calculateCommandChecksum(unsigned char length);
	void setThresholdConfig();

	unsigned char commandBuffer[BUFFERSIZE];

	UARTHandler *uartHandle;
	unsigned char moduleID, moduleIDEncoded;


};

#endif /* PROJECT_MEASUREMENT_PROTOTYPE_ULTRASONICMODULE_H_ */
