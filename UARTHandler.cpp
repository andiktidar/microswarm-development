/*
 * UARTHandler.cpp
 *
 *  Created on: 10.11.2018
 *      Author: Leon
 */

#include "UARTHandler.h"
#include "nrf_log.h"
#include "nrf_gpio.h"
#include "Err.h"
#include "nrf_delay.h"

//static event structures


UARTHandler* UARTHandler::UARTContext = nullptr;

//global uarte peripheral handles
nrfx_uarte_t uarte0(NRFX_UARTE_INSTANCE(0));
nrfx_uarte_t uarte1(NRFX_UARTE_INSTANCE(1));


void UARTHandler::readCBFunction(nrfx_uarte_event_t const *event, void* context){
	switch(event->type){
	case NRFX_UARTE_EVT_ERROR:
		NRF_LOG_INFO("UART communication error!");
		NRF_LOG_INFO("%d",event->data.error.error_mask);
		//Err::signal(20);
		break;
	case NRFX_UARTE_EVT_TX_DONE:
		UARTContext->txDone = true;
		break;
	case NRFX_UARTE_EVT_RX_DONE:
		//nrf_gpio_pin_toggle(14);
		//NRF_LOG_HEXDUMP_INFO(event->data.rxtx.p_data,event->data.rxtx.bytes);
		/*for(unsigned char count = 0; count < event->data.rxtx.bytes; count++){
			UARTContext->ringbuf.write(event->data.rxtx.p_data[count]);
		}*/
		UARTContext->rxDone = true;
		break;
	}
}




//class body
UARTHandler::UARTHandler(unsigned char instance)
: config(NRFX_UARTE_DEFAULT_CONFIG),
  rxDone(false),
  txDone(false)
{
	handle = (instance ? uarte1:uarte0);
	config.pselrxd = 8;
	config.pseltxd = 6;
	//config.hwfc = 0;
	config.baudrate = NRF_UARTE_BAUDRATE_115200;
	if(instance == 0)
		UARTContext = this;

	nrfx_uarte_init(&handle, &config, NULL/*UARTHandler::readCBFunction*/);
}


UARTHandler::UARTHandler(unsigned char instance, unsigned char txp, unsigned char rxp, nrf_uarte_baudrate_t bc)
: config(NRFX_UARTE_DEFAULT_CONFIG),
  rxDone(false),
  txDone(false)
{
	handle = (instance ? uarte1:uarte0);
	config.pselrxd = rxp;
	config.pseltxd = txp;
	//config.hwfc = 0;
	config.baudrate = bc;
	if(instance == 0)
		UARTContext = this;
	//config.use_easy_dma = false;
	nrfx_uarte_init(&handle, &config, NULL/*UARTHandler::readCBFunction*/);
}

UARTHandler::~UARTHandler() {
	nrfx_uarte_uninit(&handle);
}

void UARTHandler::write(unsigned char* data, unsigned char length){
	txDone = false;
	nrfx_uarte_tx(&handle, data, length);

	//while(nrfx_uarte_tx_in_progress(&handle)){;};
	/*unsigned char timeCounter = 0;

	while(txDone == false){
			nrf_delay_ms(1);
			timeCounter++;
			if(timeCounter > 200){
				Err::signal(24);
			}
	}*/
}

unsigned char UARTHandler::read(unsigned char* data, unsigned char length){
	rxDone = false;

	//while(!nrfx_uarte_rx_ready(&handle)){;}

	nrfx_uarte_rx(&handle, data, length);
	/*unsigned char timeCounter = 0;
	unsigned char readBytes = 0;

	while(readBytes < length){
		while(ringbuf.isAvailable() == false){
			nrf_delay_ms(1);
			timeCounter++;
			if(timeCounter > 200){
				Err::signal(23);
				return 1;
			}
		}
		data[readBytes] = ringbuf.read();
		readBytes++;
	}
	*/

	/*unsigned short timeCounter = 0;

	while(rxDone == false){
			nrf_delay_ms(1);
			timeCounter++;
			if(timeCounter > 50){
				//Err::signal(23);
				NRF_LOG_INFO("UART ERROR");
				nrfx_uarte_rx_abort(&handle);
				return 1;
			}
	}*/

	return 0;
}

void UARTHandler::setRXCallback(void* cbFunction){
	nrfx_uarte_uninit(&handle);
	Err::signalWithCode(nrfx_uarte_init(&handle,&config,((nrfx_uarte_event_handler_t) cbFunction)),28);
}



















