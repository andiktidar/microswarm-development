/*
 * Err.h
 *
 *  Created on: 20.11.2018
 *      Author: Leon
 */

#ifndef PROJECT_MEASUREMENT_PROTOTYPE_ERR_H_
#define PROJECT_MEASUREMENT_PROTOTYPE_ERR_H_

class Err {
public:
	static void signal(unsigned char number);
	static void signalWithCode(unsigned char number, unsigned char code);
};

#endif /* PROJECT_MEASUREMENT_PROTOTYPE_ERR_H_ */
