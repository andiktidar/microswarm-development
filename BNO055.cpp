/*
 * BNOInterface.cpp
 *
 *  Created on: 18.12.2018
 *      Author: Leon
 */

#include "BNO055.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "Err.h"
#include "nrf_log.h"

/*twi = NRFX_TWI_INSTANCE(0);
 nrfx_twi_config_t twiConfig = NRFX_TWI_DEFAULT_CONFIG;

 bno055_t myBNO;

 s8 BNOInterface::BNO055_I2C_bus_read(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt) {


 //unsigned char adrPlusReadFlag = 0x80 | dev_addr;

 Err::signalWithCode(nrfx_twi_tx(&twi, dev_addr, &reg_addr, 1, false), 4);
 Err::signalWithCode(nrfx_twi_rx(&twi, dev_addr, reg_data, cnt), 4);

 return 0;
 }

 s8 BNOInterface::BNO055_I2C_bus_write(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt) {


 Err::signalWithCode(nrfx_twi_tx(&twi, dev_addr, &reg_addr, 1, true), 4);
 Err::signalWithCode(nrfx_twi_tx(&twi, dev_addr, reg_data, cnt, false), 4);

 return 0;
 }

 void BNOInterface::BNO055_delay_msek(u32 msek) {
 nrf_delay_ms(msek);
 }*/

BNO055::BNO055(I2CControl* i2CControlHandle) {
	i2c = i2CControlHandle;
}

BNO055::~BNO055() {

}

unsigned char BNO055::getChipID() {
	return read8(0x00);
}

void BNO055::init() {

	reset();

	//starting the measurement mode
	nrf_delay_ms(650);
	write8(0x3d, 0x0c);
}

void BNO055::reset() {
	nrf_gpio_pin_set(14);
	write8(0x3f, 0x20);
	while (read8(0x00) != 0xa0) {
		nrf_delay_ms(10);
		NRF_LOG_INFO("%d",read8(0x00));
		nrf_delay_ms(1000);
	}
	nrf_gpio_pin_clear(14);
}

VecContainer BNO055::getVector(){
	VecContainer result;
	unsigned char buffer[6];
	unsigned short x,y,z;

	readLength(0x1a,buffer,6);

	x = (buffer[1]<<8)+buffer[0];
	y = (buffer[3]<<8)+buffer[2];
	z = (buffer[5]<<8)+buffer[4];

    result.x = (float)((double)x)/16.0;
    result.x -= ((int)(result.x/360))*360;
    result.y = (float)((double)y)/16.0;
    result.y -= ((int)(result.y/360))*360;
    result.z = (float)((double)z)/16.0;
    result.z -= ((int)(result.z/360))*360;

	return result;
}

unsigned char BNO055::read8(const unsigned char adr) {
	return i2c->read8u(0x28,adr);
}

void BNO055::readLength(const unsigned char adr, unsigned char* buffer, unsigned char length) {
	for(int count = 0; count < length; count++){
		buffer[count] = i2c->read8u(0x28, adr+count);
	}
}

void BNO055::write8(const unsigned char adr, unsigned char value) {
	i2c->write8(0x28, adr, value);
}
