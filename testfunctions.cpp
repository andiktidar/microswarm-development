/*
 * testfunctions.c
 *
 *  Created on: 12.03.2019
 *      Author: Leon
 */

#include "testfunctions.h"
#include "HF.h"
#include "GPIO.h"
#include "nrf_delay.h"
#include "BLEHandler.h"
#include "nrf_gpio.h"
#include "PIDControl.h"


#define BLE_MESSAGE_TYPE 0x04
#define BLE_DRONE_ID 0x02

#define SLEEP_TICK_MS 10
#define SLEEP_PAIRCYCLE_MS 160

#define TRIGGER_INTERVAL_MS 100

/**
 * this test method always turns the drone to face north using the bno055 sensor implementation
 */
void flyingNorthTestMethod(){
	VecContainer testVector;
	testVector = bno->getVector();
	if(testVector.x > 180)
		testVector.x -= 360;
	float tempYaw = HF::clamp(testVector.x*-0.01f, -0.25f,0.25f);

	control->setChannel(3,tempYaw);
	timeCounter = 200;
}


/**
 * this test method tries to orient the drone 90 degrees to the current measurement base.
 * This was not functional the last time it was tested due to bad threshold values on the us sensors
 */
void flying90DegTestMethod(){
	if(listenTimeout > 0){
		listenTimeout--;
	}else if(triggered){
		//usm->setRegisterValue(0x40, 0x80);
		//usm->getDataDump(false,true);
		drone->calculateDistanceAndAngleResult(usm0->getUSTravelTime(),usm1->getUSTravelTime(),10); //TODO: fix timing parameter
		triggered = false;
	}

	if(shoutTimer > 0){
		shoutTimer--;
	}else{
		isTriggering = true;
		drone->sendTrigger(isMaster);
		shoutTimer = TRIGGER_INTERVAL_MS * drone->getNumberOfDronesInSwarm()/*+1 add this when master info messages are implemented TODO!!!*/;
	}

	if(timeCounter == 0){
		timeCounter = 230;
		if(control->getChannel(5) > 0.5f){
			float diffP = HF::clamp((drone->getLastAngle()-90.0f)*0.005f,-0.2f,0.2f);
			BLEHandler::sendDebugFloat(drone->getLastAngle());
			control->setChannel(3,diffP);
		}else{
			control->freeChannel(3);
		}
	}
}

/**
 * this test method is a prototype implementation of a way to hold a specific height using the VL53L0X sensor and a PID control set
 */
void heightTestMethodVL53(){
	static float target = -1;
	static float channelFloatValue = 0;
	static PIDControl<float> heightPID(0.1f,0.01f,7.5f, 0.1f, 0.15f);

	if(timeCounter == 0){
		timeCounter = 100;
		vl53->update();

		if(control->getChannel(5) > 0.5f){
			if(target < 0){
				//target = vl53->getHeight();
				//channelFloatValue = control->getChannel(2);

				target = 0.8f;
				channelFloatValue = 0.0f;

				heightPID.reset();
			}

			BLEHandler::sendDebugFloat(vl53->getHeight());
			nrf_gpio_pin_set(LED3);
			heightPID.update(vl53->getHeight(), target,0.1f);
			float tempChannelValue = HF::clamp(heightPID.getPIDValue(),-0.15f,0.15f);
			control->setChannel(2, tempChannelValue+channelFloatValue);
		}else{
			control->freeChannel(2);
			target = -1;
			nrf_gpio_pin_clear(LED3);
		}
	}
}

/**
 * this method was used to test options in using a barometric pressure sensor for height regulation.
 * Due to problems with the board temperature fluctuating, the BMP388 sensor was found to be insufficient for height regulation
 */
void heightTestMethodBMP388(){
	static double target = -1;
	static float channelFloatValue = 0;

	//NRF_LOG_INFO("" NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(control->getChannel(5)));

	if(timeCounter <= 0){
		bmp->update();

		float currentPressure = bmp->getPressure();

		if(control->getChannel(5) > 0.5f){
			if(target < 0){
				target = currentPressure;
				channelFloatValue = control->getChannel(2);
			}

			BLEHandler::sendDebugFloat(currentPressure);
			nrf_gpio_pin_set(LED3);
			float tempChannelValue = HF::clamp((currentPressure-target)/100.0f,-0.05f,0.05f);
			control->setChannel(2, tempChannelValue+channelFloatValue);
		}else{
			control->freeChannel(2);
			target = -1;
			nrf_gpio_pin_clear(LED3);
		}

		timeCounter = 100;
	}
}

/**
 * this method was used to identify threshold issues regarding motor noise in the ultrasonic sensor profile
 * It gradually speeds up the motors and performs a measurement cycle with the current ultrasonic configuration.
 * The results are printed via the debug console.
 */
void thresholdMeasurementTestMethod(){
	static short txTimer = 1;
	static short increaseTimer = 1;
	static float speed = -1.0f;
	static short listenTimer = 251;
	static short resultTimer = 751;
	static bool active = false;
	static bool armed = false;

	txTimer--;
	increaseTimer--;
	listenTimer--;
	resultTimer--;

	if(increaseTimer == 0){
		increaseTimer = 1000;
		if(!active){
			nrf_delay_ms(2000);
			//pitch
			control->setChannel(0,0.0f);
			//roll
			control->setChannel(1,0.0f);
			//speed
			control->setChannel(2,-1.0f);
			//yaw
			control->setChannel(3,0.0f);
			//arm
			control->setChannel(4,-1.0f);
			//normal mode
			control->setChannel(5,-1.0f);

			//other channels

			control->setChannel(6,0.0f);
			control->setChannel(7,0.0f);
			control->setChannel(8,0.0f);
			control->setChannel(9,0.0f);
			control->setChannel(10,0.0f);
			control->setChannel(11,0.0f);
			control->setChannel(12,0.0f);
			control->setChannel(13,0.0f);
			active = true;
		}else if(!armed){
			//arm the quad
			control->setChannel(4,1.0f);
			armed = true;
		}
		else{
			speed = HF::clamp((speed+0.2f),-1,1);
			//speed
			control->setChannel(2,speed);
			if(speed > 0.8f){
				control->setChannel(4,-1.0f);
			}
		}
		//set dump flag
		usm0->setRegisterValue(0x40, 0x80);
	}
	//send channel signals
	if(txTimer <= 0){
		control->pushControlMessage();
		txTimer = 6;
	}
	if(listenTimer == 0){
		listenTimer = 1000;
		usm0->measure(false);
	}
	if(resultTimer == 0){
		resultTimer = 1000;
		usm0->printDataDump(false,true);
	}

}





/**
 * this test method periodically prints the mean motor noise level via BLE debug messages once the mode switch is flipped to 1.0f.
 * It can be used to get an idea of the current noise pollution on the ultrasonic sensors using the current gain configuration.
 */
void inFlightThresMeasurementTestMethod(){
	static short listenTimer = 251;
	static short resultTimer = 751;

	if(control->getChannel(5) > 0.5f){
		listenTimer--;
		resultTimer--;
		if(listenTimer == 50){
			//set dump flag
			usm0->setRegisterValue(0x40, 0x80);
		}
		if(listenTimer < 0){
			listenTimer += 1000;
			usm0->measure(false);
		}
		if(resultTimer < 0){
			resultTimer += 1000;
			//BLEHandler::sendDebugInt(usm0->getNoiseLevelFromMeasurement());
			usm0->printDataDump(true,false);
		}
	}

}






















