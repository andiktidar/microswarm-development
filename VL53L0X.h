/*
 * VL53L0X.h
 *
 *  Created on: 07.03.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_VL53L0X_H_
#define MICROSWARM_V1_VL53L0X_H_
#include "I2CControl.h"
#include "vl53l0x_api.h"
#include "vl53l0x_platform.h"
#include "vl53l0x_i2c_platform.h"

class VL53L0X {
public:
	VL53L0X(I2CControl* i2CControlHandle);
	~VL53L0X();

	void init();
	void update();
	float getHeight();

private:

	VL53L0X_Dev_t vlDevice;
	float lastHeight;
	VL53L0X_RangingMeasurementData_t rangingData;
};

#endif /* MICROSWARM_V1_VL53L0X_H_ */
