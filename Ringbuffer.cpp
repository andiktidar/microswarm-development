/*
 * Ringbuffer.cpp
 *
 *  Created on: 15.12.2018
 *      Author: Leon
 */

#include "Ringbuffer.h"
#include "Err.h"
#include "nrf_log.h"

Ringbuffer::Ringbuffer(unsigned char size)
: bufferSize(size),
  readIndex(0),
  writeIndex(0),
  available(0)
{
	if(bufferSize > 256){
		NRF_LOG_INFO("buffer can only be 256bytes or less!");
		Err::signal(22);
	}
	buffer = new unsigned char[size];
}

Ringbuffer::~Ringbuffer() {
	delete[] buffer;
}

unsigned char Ringbuffer::isAvailable(){
	return available;
}

unsigned char Ringbuffer::read(){
	if(readIndex == writeIndex){
		return 0;
	}
	unsigned char result = buffer[readIndex];
	readIndex = (readIndex+1)%bufferSize;
	return result;
}

void Ringbuffer::write(unsigned char value){
	if(writeIndex == (readIndex-1) || (writeIndex == (bufferSize-1) && readIndex == 0)){
		NRF_LOG_INFO("ring buffer overflow");
		Err::signal(21);
	}
	buffer[writeIndex] = value;
	writeIndex = (writeIndex+1)%bufferSize;
	available++;
}
