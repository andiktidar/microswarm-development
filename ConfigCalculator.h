/*
 * ConfigCalculator.h
 *
 *  Created on: 11.12.2018
 *      Author: Leon
 */

#ifndef PROJECT_MEASUREMENT_PROTOTYPE_CONFIGCALCULATOR_H_
#define PROJECT_MEASUREMENT_PROTOTYPE_CONFIGCALCULATOR_H_

class ConfigCalculator {
public:
	static unsigned char calcFrequencyByte(unsigned char khz);
	static unsigned char calcBurstCyclesByte(unsigned char numberOfBurstCycles);
	static unsigned char calcRecordTimeByte(unsigned char recTimeMS);
};

#endif /* PROJECT_MEASUREMENT_PROTOTYPE_CONFIGCALCULATOR_H_ */
