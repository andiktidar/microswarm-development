/*
 * testfunctions.h
 *
 *  Created on: 12.03.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_TESTFUNCTIONS_H_
#define MICROSWARM_V1_TESTFUNCTIONS_H_


#include "BNO055.h"
#include "BMP388.h"
#include "VL53L0X.h"
#include "Drone.h"
#include "UltrasonicModule.h"
#include "ControlMultiplexer.h"

extern BNO055* bno;
extern BMP388* bmp;
extern unsigned int timeCounter;
extern Drone* drone;
extern UltrasonicModule* usm0;
extern UltrasonicModule* usm1;
extern ControlMultiplexer* control;
extern VL53L0X* vl53;

extern unsigned char listenTimeout;
extern bool triggered;
extern short shoutTimer;
extern bool isTriggering;

extern bool enterPressed;

extern bool isMaster;


void thresholdMeasurementTestMethod();
void heightTestMethodBMP388();
void heightTestMethodVL53();
void flying90DegTestMethod();
void flyingNorthTestMethod();
void inFlightThresMeasurementTestMethod();

#endif /* MICROSWARM_V1_TESTFUNCTIONS_H_ */
