/*
 * I2CControl.h
 *
 *  Created on: 20.02.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_I2CCONTROL_H_
#define MICROSWARM_V1_I2CCONTROL_H_

#include "nrfx_twim.h"

class I2CControl {
public:
	I2CControl();
	virtual ~I2CControl();

	unsigned char read8u(const unsigned char chipID, const unsigned char adr);
	char read8(const unsigned char chipID, const unsigned char adr);
	void readLength(const unsigned char chipID, const unsigned char adr, unsigned char* buffer, unsigned char length);
	void writeLength(const unsigned char chipID, const unsigned char adr, unsigned char* buffer, unsigned char length);
	void write8(const unsigned char chipID, const unsigned char adr, unsigned char value);

	unsigned short read16u(const unsigned char chipID, const unsigned char adr);
	unsigned short read32u(const unsigned char chipID, const unsigned char adr);
	short read16(const unsigned char chipID, const unsigned char adr);

private:

	nrfx_twim_t twi;
	nrfx_twim_config_t twiConfig;
};

#endif /* MICROSWARM_V1_I2CCONTROL_H_ */
