/*
 * BLEHandler.h
 *
 *  Created on: 19.11.2018
 *      Author: Leon
 */

#ifndef PROJECT_MEASUREMENT_PROTOTYPE_BLEHANDLER_H_
#define PROJECT_MEASUREMENT_PROTOTYPE_BLEHANDLER_H_

#include "nrf.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_scan.h"

class BLEHandler {
public:
	static void init(unsigned char key = 0x01, void* cbr = NULL, void* cbt = NULL);
	static void send(unsigned char *data, unsigned char length);
	static void setCBTFunction(void * cb);
	static void setCBRFunction(void * cb);
	static void changeID(unsigned char newID);
	static unsigned char* getMessageData();

	//debug functions in conjunction with the android app
	static void sendDebugFloat(float value);
	static void sendDebugInt(int value);
	static void sendDebugFull(float mx, float my, float vx, float vy, float sx, float sy);
	static void sendDebugDistanceAndAngle(float distance, float angle);
	static void floatToCharArray(float value, unsigned char* data);

private:
	static void TXHandler(bool radioEvent);
	static void RXHandler(ble_evt_t const * p_ble_evt, void * p_context);
	static void SEHandler(scan_evt_t const * p_scan_evt);
	static void endTransmission();
	static void callbackDummy();

	static unsigned char deviceKey[2];
	static ble_gap_adv_data_t encodedMessage;
	static ble_advdata_t advDataConfig;
	static ble_advdata_manuf_data_t msgData;
	static unsigned char msgDataArray[29];
	static ble_gap_adv_params_t advParamSet;
	static unsigned char resultingDataset[31];
	static uint8_t advHandle;
	static uint8_t error;

	static ble_gap_scan_params_t scanParams;
	static nrf_ble_scan_init_t scanInitSet;
	static bool currentlySending;
	static ble_gap_addr_t addressFilter;

	static void* transmitEventCallback;
	static void* receiveEventCallback;

	static bool initialized;

	static unsigned char receivedData[32];
};

#endif /* PROJECT_MEASUREMENT_PROTOTYPE_BLEHANDLER_H_ */
