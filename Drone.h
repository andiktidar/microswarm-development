/*
 * Drone.h
 *
 *  Created on: 17.01.2019
 *      Author: Leon
 */

#ifndef MICROSWARM_V1_DRONE_H_
#define MICROSWARM_V1_DRONE_H_

class Drone {
public:
	Drone();
	~Drone();

	void setDroneList(unsigned char* msg);
	void setDroneList(unsigned char* protoList, unsigned char numOfProtoDrones);
	unsigned char getID();
	unsigned char getIndex();



	void sendACK();
	void sendTrigger(bool master);
	void sendCall();
	void sendDroneList();
	unsigned char getNumberOfDronesInSwarm();
	void calculateDistanceResult(unsigned short distUS);
	void calculateDistanceAndAngleResult(unsigned short distUS1, unsigned short distUS2, unsigned int time);

	float getLastAngle();
	float getLastDistance();

	void updateVirtualDrone(float steerX, float steerY, float deltaTime);
	float getXPos();
	float getYPos();
	float getXSpd();
	float getYSpd();

	void setZPos(float z);

	void resetVirtualDrone();
	bool isReady();

	//test functions for trigonometry approach
	void setReferenceID(unsigned char rid);
	unsigned char getReferenceID();
	void calculateTrigonometryPosResult(unsigned short distUS1, unsigned int time = 0);

private:

	float generateVirtualAngle();
	void integrateMeasuredPos(float measX, float measY);

	unsigned char id, index;
	unsigned char* droneList;
	unsigned char numOfDrones;

	unsigned char status;

	float  posX, posY, posZ, speedX,speedY;

	float lastMeasPosX, lastMeasPosY, posDevSpanX, posDevSpanY;
	float measDeltaTimeX, measDeltaTimeY;

	//temporary test variable to store the last measured distance;
	unsigned short lastDistance;
	float lastAngle;

	//testing variables for trigonometry approach
	unsigned char referenceID;
	float trigDistances[2];



};

#endif /* MICROSWARM_V1_DRONE_H_ */
